VERSION=`git describe --tags`
BUILD_TIME=`date +%FT%T%z`
LDFLAGS=-ldflags "-X main.Version=${VERSION} -X main.BuildTime=${BUILD_TIME}"

run: clear
	go build ${LDFLAGS} -o ./bin/${SER} ./service/${SER}/main.go
	./bin/${SER}

dispatch: 
	gcloud config set project kalimasi
	gcloud app deploy dispatch.yaml

clear:
	rm -rf ./bin/$(SER)