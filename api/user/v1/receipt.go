package v1

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/model"
	"kalimasi/rsrc"
	"kalimasi/util"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"cloud.google.com/go/storage"
	"golang.org/x/oauth2/google"
)

type ReceiptAPI string

func (api ReceiptAPI) GetName() string {
	return string(api)
}

func (a ReceiptAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		{Path: "/v1/receipt", Next: a.createEndpoint, Method: "POST", Auth: true},
		{Path: "/v1/receipt", Next: a.getEndpoint, Method: "GET", Auth: true},
		{Path: "/v1/receipt/accTerm/_replace", Next: a.replaceEndpoint, Method: "POST", Auth: true},
		{Path: "/v1/receipt/{ID}", Next: a.detailEndpoint, Method: "GET", Auth: true},
		{Path: "/v1/receipt/{ID}", Next: a.modifyEndpoint, Method: "PUT", Auth: true},
		{Path: "/v1/receipt/{ID}", Next: a.delEndpoint, Method: "DELETE", Auth: true},
		{Path: "/v1/receipt/einvoice/{ID}", Next: a.createFromEInvEndpoint, Method: "POST", Auth: true},
		{Path: "/v1/receipt/{ID}/image", Next: a.createReceiptImgEndpoint, Method: "POST", Auth: true},
		{Path: "/v1/einvoice", Next: a.getEInvEndpoint, Method: "GET", Auth: true},
		{Path: "/v1/einvoice/_sync", Next: a.syncEInvEndpoint, Method: "POST", Auth: true},
		{Path: "/v1/einvoice/{ID}", Next: a.getEInvDetailEndpoint, Method: "GET", Auth: true},
	}
}

func (a ReceiptAPI) Init() {

}

type UploadImg struct {
	Ext         string `json:"ext"`
	ContentType string `json:"contentType"`
}

func (api *ReceiptAPI) createReceiptImgEndpoint(w http.ResponseWriter, req *http.Request) {
	pv := util.GetPathVars(req, []string{"ID"})
	key := pv["ID"]
	ui := &UploadImg{}
	err := json.NewDecoder(req.Body).Decode(ui)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	di := rsrc.GetDI()

	jsonKey, err := ioutil.ReadFile(di.GetGCPCredentialPath())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	conf, err := google.JWTConfigFromJSON(jsonKey)

	url, err := storage.SignedURL(os.Getenv("BUCKET"), fmt.Sprintf("testreceipt/%s.%s", key.(string), ui.Ext),
		&storage.SignedURLOptions{
			GoogleAccessID: conf.Email,
			Method:         "PUT",
			PrivateKey:     conf.PrivateKey,
			Expires:        time.Now().Add(15 * time.Minute),
			ContentType:    ui.ContentType,
		})
	w.Header().Set("Location", url)
	w.Write([]byte("OK"))
}

func (api *ReceiptAPI) replaceEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.AccTermRepl{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	cb.CompanyId = ui.CompanyID
	di := rsrc.GetDI()
	dbclt := di.GetMongoByReq(req)
	atm := model.GetAccTermModel(dbclt)

	var actTerm *doc.AccTerm
	for k := range doc.TypeMapCode {
		actTerm, err = atm.FindAccTerm(ui.CompanyID, k, cb.Replace)
		if actTerm != nil {
			break
		}
	}
	if actTerm == nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("replace accterm code not found: " + cb.Replace))
		return
	}

	rm := model.GetReceiptModel(dbclt, di.GetFBStorage())
	r, err := rm.ReplaceAccterm(cb, actTerm, ui)
	if len(r) > 0 {
		w.WriteHeader(http.StatusPartialContent)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *ReceiptAPI) syncEInvEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.SyncEInv{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)

	d := rsrc.GetDI()
	mgoDB := model.GetMgoDBModelByReq(req)
	log := d.GetLog()
	syncNumax := model.NewNumaxSync(d.NumaxConf, mgoDB, log)
	err = syncNumax.SyncByCompany(ui.CompanyID, cb.Date)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (api *ReceiptAPI) createFromEInvEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.TrasferEInv{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	bs := &doc.EInvoice{ID: qid, CompanyID: ui.CompanyID}
	dbm := model.GetMgoDBModelByReq(req)
	err = dbm.FindByID(bs)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	cb.SetEInv(bs)
	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	// 是否要審核
	if model.CompanyAudit(cb, w, req) {
		return
	}

	di := rsrc.GetDI()
	dbclt := di.GetMongoByReq(req)
	rm := model.GetReceiptModel(dbclt, di.GetFBStorage())
	rid, err := rm.Create(&cb.CreateReceipt, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	bs.ReceiptID = &rid
	err = dbm.Update(bs, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("update receipt fail: " + rid.Hex()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *ReceiptAPI) getEInvDetailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	bs := &doc.EInvoice{ID: qid, CompanyID: ui.CompanyID}
	di := rsrc.GetDI()
	mgo := di.GetMongoByReq(req)
	dbm := model.GetMgoDBModel(mgo)
	err = dbm.FindByID(bs)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	out, _ := doc.Format(bs, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.EInvoice); ok {

			am := model.GetAuditModel(mgo)
			ad := am.GetAuditDoc(d, ui.CompanyID)
			inp := &input.TrasferEInv{}
			state := "none"
			if ad != nil {
				state = ad.State
				json.Unmarshal(ad.SerialJSON, inp)
			}

			rid := ""
			if d.ID.Valid() {
				rid = d.ID.Hex()
			}
			return map[string]interface{}{
				"id":        d.ID.Hex(),
				"number":    d.VoucherNumber,
				"vatNumber": d.VatNumber,
				"date":      d.DateTime.Format(time.RFC3339),
				"amount":    d.Amount,
				"tax":       d.Tax,
				"taxType":   d.TaxType,
				"source":    d.Source,
				"imgUrl":    d.ImgUrl,
				"detail":    d.Detail,
				"receiptId": rid,
				"auditInfo": map[string]interface{}{
					"state":   state,
					"content": inp,
				},
			}
		}
		return nil
	})
	if out == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *ReceiptAPI) getEInvEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"state", "sd", "ed", "page", "limit"}, true)
	ui := input.GetUserInfo(req)
	qb := input.QueryEInv{
		State:     qv["state"].(string),
		StartDate: qv["sd"].(string),
		EndDate:   qv["ed"].(string),
	}
	if err := qb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	bs := &doc.EInvoice{CompanyID: ui.CompanyID}
	dbm := model.GetMgoDBModelByReq(req)

	page, err := strconv.Atoi(qv["page"].(string))
	if err != nil {
		page = 1
	}

	limit, err := strconv.Atoi(qv["limit"].(string))
	if err != nil || limit > 200 {
		limit = 200
	}
	out, err := dbm.Paginator(bs, qb.GetMgoQuery(), limit, page, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.EInvoice); ok {
			receiptID := ""
			if d.ReceiptID != nil {
				receiptID = d.ReceiptID.Hex()
			}
			return map[string]interface{}{
				"id":        d.ID.Hex(),
				"date":      d.DateTime.Format(time.RFC3339),
				"number":    d.VoucherNumber,
				"amount":    d.Amount,
				"source":    d.Source,
				"receiptId": receiptID,
			}
		}
		return nil
	})
	if err != nil {
		if err.Error() == "query data is 0" {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *ReceiptAPI) delEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	bs := &doc.Receipt{ID: qid, CompanyID: ui.CompanyID}
	di := rsrc.GetDI()
	dbclt := di.GetMongoByReq(req)
	bm := model.GetReceiptModel(dbclt, di.GetFBStorage())
	err = bm.Delete(bs, ui)
	if err != nil {
		switch err.(type) {
		case rsrc.ApiError:
			ae := err.(rsrc.ApiError)
			w.WriteHeader(ae.StatusCode)
			w.Write([]byte(ae.Error()))
		default:
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		return
	}
	w.Write([]byte("ok"))
}

func (api *ReceiptAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	bs := &doc.Receipt{ID: qid, CompanyID: ui.CompanyID}
	di := rsrc.GetDI()
	mgo := di.GetMongoByReq(req)
	mgoDB := model.GetMgoDBModel(mgo)
	err = mgoDB.FindByID(bs)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}

	out, _ := doc.Format(bs, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.Receipt); ok {
			bid := ""
			if d.BudgetId != nil {
				bid = d.BudgetId.Hex()
			}
			var debit []map[string]interface{}
			for _, da := range d.DebitAccTerm {
				debit = append(debit, map[string]interface{}{
					"id":     da.ID.Hex(),
					"code":   da.Code,
					"name":   da.Name,
					"amount": da.Amount,
					"desc":   da.Desc,
				})
			}
			var credit []map[string]interface{}
			for _, da := range d.CreditAccTerm {
				credit = append(credit, map[string]interface{}{
					"id":     da.ID.Hex(),
					"code":   da.Code,
					"name":   da.Name,
					"amount": da.Amount,
					"desc":   da.Desc,
				})
			}
			pictures := []map[string]interface{}{}
			for _, p := range d.Pictures {
				sp := strings.Split(p, "%2F")
				l := len(sp)
				sp[l-1] = util.StrAppend("thumb_", sp[l-1])
				pictures = append(pictures, map[string]interface{}{
					"origin": p,
					"thumb":  strings.Join(sp, "%2F"),
				})
			}
			am := model.GetAuditModel(mgo)
			state := am.GetAuditState(d, ui.CompanyID)
			return map[string]interface{}{
				"id":              d.ID.Hex(),
				"budgetId":        bid,
				"accType":         d.TypeAcc,
				"rDate":           d.DateTime.Format(time.RFC3339),
				"type":            d.Typ,
				"number":          d.Number,
				"VATnumber":       d.VATnumber,
				"amount":          d.Amount,
				"isCollect":       d.IsCollect,
				"collectQuantity": d.CollectQuantity,
				"isFixedAsset":    d.IsFixedAsset,
				"tax": map[string]interface{}{
					"type":   d.TaxInfo.Typ,
					"amount": d.TaxInfo.Amount,
					"rate":   d.TaxInfo.Rate,
				},
				"debitAccTermList":  debit,
				"creditAccTermList": credit,
				"pictures":          pictures,
				"auditInfo": map[string]interface{}{
					"state": state,
				},
			}
		}
		return nil
	})
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *ReceiptAPI) modifyEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.PutReceipt{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)
	if err != nil || qid != cb.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	// 是否要審核
	if model.CompanyAudit(cb, w, req) {
		return
	}

	ui := input.GetUserInfo(req)
	di := rsrc.GetDI()
	dbclt := di.GetMongoByReq(req)
	bm := model.GetReceiptModel(dbclt, di.GetFBStorage())
	err = bm.Modify(cb, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *ReceiptAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"at", "t", "number", "budget", "sd", "ed", "code", "page", "limit"}, true)
	ui := input.GetUserInfo(req)
	qb := input.QueryReceipt{
		AccType:   qv["at"].(string),
		Typ:       qv["t"].(string),
		Number:    qv["number"].(string),
		BudgetID:  qv["budget"].(string),
		Code:      qv["code"].(string),
		StartDate: qv["sd"].(string),
		EndDate:   qv["ed"].(string),
		CompanyID: ui.CompanyID,
	}

	if err := qb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	page, err := strconv.Atoi(qv["page"].(string))
	if err != nil {
		page = 1
	}

	limit, err := strconv.Atoi(qv["limit"].(string))
	if err != nil || limit > 200 {
		limit = 200
	}

	q := qb.GetMgoQuery()
	mgodb := model.GetMgoDBModelByReq(req)

	r := &doc.Receipt{
		CompanyID: ui.CompanyID,
	}
	out, err := mgodb.PaginatorPipeline(r, r.GetPipeline(q), limit, page, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.Receipt); ok {
			return map[string]interface{}{
				"id":         d.ID.Hex(),
				"accType":    d.TypeAcc,
				"date":       d.DateTime.Format(time.RFC3339),
				"number":     d.Number,
				"VATnumber":  d.VATnumber,
				"amount":     d.Amount,
				"type":       d.Typ,
				"createTime": d.CreateTime.Format(time.RFC3339),
				"auditInfo": map[string]interface{}{
					"state": d.GetAuditState(),
				},
			}
		}
		return nil
	})
	if err != nil {
		if err.Error() == "query data is 0" {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *ReceiptAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {
	cr := &input.CreateReceipt{}
	err := json.NewDecoder(req.Body).Decode(cr)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cr.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	// 是否要審核
	if model.CompanyAudit(cr, w, req) {
		return
	}

	ui := input.GetUserInfo(req)
	di := rsrc.GetDI()
	dbclt := di.GetMongoByReq(req)
	rm := model.GetReceiptModel(dbclt, di.GetFBStorage())
	rid, err := rm.Create(cr, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte(rid.Hex()))
}
