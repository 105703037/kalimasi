package v1

import (
	"encoding/json"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/model"
	"kalimasi/rsrc"
	"kalimasi/util"
	"net/http"
	"strconv"
	"time"
)

type TransferAPI string

func (api TransferAPI) GetName() string {
	return string(api)
}

func (a TransferAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		{Path: "/v1/transfer", Next: a.createEndpoint, Method: "POST", Auth: true},
		{Path: "/v1/transfer", Next: a.getEndpoint, Method: "GET", Auth: true},
		{Path: "/v1/transfer/accTerm/_replace", Next: a.replaceTransferEndpoint, Method: "POST", Auth: true},
		{Path: "/v1/transfer/{ID}", Next: a.detailEndpoint, Method: "GET", Auth: true},
		{Path: "/v1/transfer/{ID}", Next: a.modifyEndpoint, Method: "PUT", Auth: true},
		{Path: "/v1/transfer/{ID}", Next: a.delEndpoint, Method: "DELETE", Auth: true},
	}
}

func (a TransferAPI) Init() {

}

func (api *TransferAPI) replaceTransferEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.AccTermRepl{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	cb.CompanyId = ui.CompanyID
	di := rsrc.GetDI()
	dbclt := di.GetMongoByReq(req)
	atm := model.GetAccTermModel(dbclt)

	var actTerm *doc.AccTerm
	for k := range doc.TypeMapCode {
		actTerm, err = atm.FindAccTerm(ui.CompanyID, k, cb.Replace)
		if actTerm != nil {
			break
		}
	}
	if actTerm == nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("replace accterm code not found: " + cb.Replace))
		return
	}

	rm := model.GetTransferModel(dbclt)
	r, err := rm.ReplaceAccterm(cb, actTerm, ui)
	if len(r) > 0 {
		w.WriteHeader(http.StatusPartialContent)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *TransferAPI) delEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	bs := &doc.Transfer{ID: qid, CompanyID: ui.CompanyID}
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	bm := model.GetTransferModel(dbclt)
	err = bm.Delete(bs, ui)
	if err != nil {
		switch err.Error() {
		case "not found":
			w.WriteHeader(http.StatusNotFound)
		default:
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		return
	}
	w.Write([]byte("ok"))
}

func (api *TransferAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	bs := &doc.Transfer{ID: qid, CompanyID: ui.CompanyID}
	di := rsrc.GetDI()
	mgo := di.GetMongoByReq(req)
	mgoDB := model.GetMgoDBModel(mgo)
	err = mgoDB.FindByID(bs)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}

	out, _ := doc.Format(bs, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.Transfer); ok {
			bid := ""
			if d.BudgetId != nil {
				bid = d.BudgetId.Hex()
			}
			am := model.GetAuditModel(mgo)
			state := am.GetAuditState(d, ui.CompanyID)
			return map[string]interface{}{
				"id":            d.ID.Hex(),
				"summary":       d.Summary,
				"budgetId":      bid,
				"tDate":         d.DateTime.Format(time.RFC3339),
				"debitAccTerm":  d.DebitAccTerm,
				"creditAccTerm": d.CreditAccTerm,
				"auditInfo": map[string]interface{}{
					"state": state,
				},
			}
		}
		return nil
	})
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *TransferAPI) modifyEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.PutTransfer{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)
	if err != nil || qid != cb.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	// 是否要審核
	if model.CompanyAudit(cb, w, req) {
		return
	}

	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	bm := model.GetTransferModel(dbclt)
	err = bm.Modify(cb, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *TransferAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"code", "s", "budget", "sd", "ed", "page", "limit"}, true)
	ui := input.GetUserInfo(req)
	qb := input.QueryTransfer{
		Summary:   qv["s"].(string),
		BudgetId:  qv["budget"].(string),
		StartDate: qv["sd"].(string),
		EndDate:   qv["ed"].(string),
		Code:      qv["code"].(string),
		CompanyID: ui.CompanyID,
	}
	if err := qb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	page, err := strconv.Atoi(qv["page"].(string))
	if err != nil {
		page = 1
	}

	limit, err := strconv.Atoi(qv["limit"].(string))
	if err != nil || limit > 200 {
		limit = 200
	}

	q := qb.GetMgoQuery()
	mgodb := model.GetMgoDBModelByReq(req)
	d := &doc.Transfer{CompanyID: ui.CompanyID}
	out, err := mgodb.PaginatorPipeline(d, d.GetPipeline(q), limit, page, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.Transfer); ok {
			budget := ""
			if len(d.Budget) == 1 {
				budget = d.Budget[0].Title
			}
			amount := 0
			for _, c := range d.CreditAccTerm {
				amount += c.Amount
			}
			return map[string]interface{}{
				"id":         d.ID.Hex(),
				"budget":     budget,
				"desc":       d.Summary,
				"amount":     amount,
				"tDate":      d.DateTime.Format(time.RFC3339),
				"createTime": d.CreateTime.Format(time.RFC3339),
				"auditInfo": map[string]interface{}{
					"state": d.GetAuditState(),
				},
			}
		}
		return nil
	})
	if err != nil {
		if err.Error() == "query data is 0" {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *TransferAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {
	cr := &input.CreateTransfer{}
	err := json.NewDecoder(req.Body).Decode(cr)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cr.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	// 是否要審核
	if model.CompanyAudit(cr, w, req) {
		return
	}

	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	rm := model.GetTransferModel(dbclt)
	err = rm.Create(cr, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}
