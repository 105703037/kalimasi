package v1

import (
	"encoding/json"
	"net/http"

	"kalimasi/input"
	"kalimasi/model"
	"kalimasi/rsrc"
)

type AuthAPI string

func (api AuthAPI) GetName() string {
	return string(api)
}

func (a AuthAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		&rsrc.APIHandler{Path: "/v1/signup", Next: a.signupEndpoint, Method: "POST", Auth: false},
		&rsrc.APIHandler{Path: "/info", Next: a.infoEndpoint, Method: "GET", Auth: false},
	}
}

func (a AuthAPI) Init() {

}

func (api *AuthAPI) infoEndpoint(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("consult"))
}

func (api *AuthAPI) signupEndpoint(w http.ResponseWriter, req *http.Request) {
	ccs := &input.CreateConsult{}
	err := json.NewDecoder(req.Body).Decode(ccs)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = ccs.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	csm := model.GetConsultModel(dbclt)

	if csm.IsConsultExist(ccs.Account) {
		w.WriteHeader(http.StatusConflict)
		w.Write([]byte("company is exist"))
		return
	}

	// 是否要系統審核
	if model.SystemAudit(ccs, w, req) {
		return
	}

	// 不需審核 or 審核通過直接建立

	if err = csm.Create(ccs, ui); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}
