package model

import (
	"errors"
	"fmt"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/rsrc"
	"kalimasi/rsrc/log"
	"kalimasi/rsrc/mail"
	"kalimasi/util"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type companyModel struct {
	dbmodel *mgoDBModel
	log     *log.Logger
}

func GetCompanyModel(mgodb *mgo.Database) *companyModel {
	mongo := GetMgoDBModel(mgodb)
	return &companyModel{dbmodel: mongo, log: mongo.log}
}

func (cm *companyModel) Create(input *input.CreateCompany, u *input.ReqUser) error {
	uid, err := doc.GetObjectID(u.ID)
	if err != nil {
		return errors.New("invalid user id")
	}
	cpi := doc.ContactPersonInfo{
		Name: input.ContactPerson.Name,
	}
	cpi.AddContact(input.ContactPerson.Contact.CtType, input.ContactPerson.Contact.Value)
	c := &doc.Company{
		Typ:       input.Typ,
		UnitCode:  input.VATnumber,
		Name:      input.Name,
		TaxRate:   5,
		Fax:       input.Fax,
		OwnerName: input.Owner,
		AddressInfo: doc.AddressInfo{
			PostalCode: input.AddressInfo.PostalCode,
			Country:    input.AddressInfo.Country,
			City:       input.AddressInfo.City,
			Address:    input.AddressInfo.Address,
		},
		ContactPersonInfo: cpi,
		TaxInfo: doc.CompanyTaxInfo{
			Code: input.TaxCode,
		},
	}
	err = cm.dbmodel.Save(c, u)
	if err != nil {
		return err
	}

	up := &doc.UserCompanyPerm{
		UserID:     uid,
		CompanyID:  c.ID,
		Permission: doc.UserPermOwn,
	}
	return cm.dbmodel.Save(up, u)
}

func (cm *companyModel) CreateAccTerm(bi *input.CreateAccTermItem, u *input.ReqUser) error {

	r := &doc.AccTermList{
		Typ:       bi.Typ,
		CompanyID: u.CompanyID,
	}

	return cm.dbmodel.Save(r, u)
}

func (cm *companyModel) CreatePayMethod(pi *input.PayAccTermMapConf, u *input.ReqUser) error {
	bs := &doc.Company{ID: u.CompanyID}
	err := cm.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}
	/*從資料庫取得原始資料*/
	cm.log.Warn(fmt.Sprintln(bs.Incomes))
	bs.AddPayMethod(pi.GetAccDoc())
	err = cm.dbmodel.Update(bs, u)
	if err != nil {
		return err
	}
	return cm.dbmodel.addDocLog(bs, u, doc.ActUpdate)

}

func (cm *companyModel) ModifyPayMethod(pi *input.PutPayAccTermMapConf, u *input.ReqUser) error {
	bs := &doc.Company{ID: pi.ID}
	err := cm.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}
	/*從資料庫取得原始資料*/
	um := bs
	err = bs.ChangePayMethod(pi.GetAccDoc())
	if err != nil {
		return err
	}

	err = cm.dbmodel.Update(um, u)
	if err != nil {
		return err
	}
	return cm.dbmodel.addDocLog(bs, u, doc.ActUpdate)

}

func (cm *companyModel) DeletePayMethod(methodName string, u *input.ReqUser) error {
	bs := &doc.Company{ID: u.CompanyID}
	err := cm.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}
	/*從資料庫取得原始資料*/
	um := bs
	err = bs.DeletePayMethod(methodName)
	if err != nil {
		return err
	}
	err = cm.dbmodel.Update(um, u)
	if err != nil {
		return err
	}
	return cm.dbmodel.addDocLog(bs, u, doc.ActUpdate)

}

func (cm *companyModel) CreateIncome(pi *input.PayAccTermMapConf, u *input.ReqUser) error {
	bs := &doc.Company{ID: u.CompanyID}
	err := cm.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}
	cm.log.Warn(fmt.Sprintln(bs.Incomes))
	/*從資料庫取得原始資料*/
	bs.AddIncome(pi.GetAccDoc())

	err = cm.dbmodel.Update(bs, u)
	if err != nil {
		return err
	}
	return cm.dbmodel.addDocLog(bs, u, doc.ActUpdate)

}

func (cm *companyModel) ModifyIncome(pi *input.PutPayAccTermMapConf, u *input.ReqUser) error {
	bs := &doc.Company{ID: pi.ID}
	err := cm.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}

	/*從資料庫取得原始資料*/
	um := bs
	err = bs.ChangeIncome(pi.GetAccDoc())
	if err != nil {
		return err
	}

	err = cm.dbmodel.Update(um, u)
	if err != nil {
		return err
	}
	return cm.dbmodel.addDocLog(bs, u, doc.ActUpdate)

}
func (cm *companyModel) DeleteIncome(incomeName string, u *input.ReqUser) error {
	bs := &doc.Company{ID: u.CompanyID}
	err := cm.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}
	/*從資料庫取得原始資料*/
	um := bs
	err = bs.DeleteIncome(incomeName)
	if err != nil {
		return err
	}

	err = cm.dbmodel.Update(um, u)
	if err != nil {
		return err
	}
	return cm.dbmodel.addDocLog(bs, u, doc.ActUpdate)

}

func (cm *companyModel) CreateFakeCompany(u *doc.User) error {
	if !u.ID.Valid() {
		return errors.New("invalid user")
	}
	rand.Seed(time.Now().UnixNano())
	fakeUnitCode := fmt.Sprintf("%08d", (rand.Intn(99999999)))
	isFake := true
	companyID, err := doc.GetCompanyId(fakeUnitCode, isFake)
	if err != nil {
		return err
	}

	c := &doc.Company{
		Typ:      doc.CompanyTypEnterprise,
		Name:     fmt.Sprintf("%s的測試公司", u.DisplayName),
		UnitCode: fakeUnitCode,
		IsFake:   true,
		TaxRate:  5,
	}

	err = cm.dbmodel.Save(c, u)

	uc := &doc.UserCompanyPerm{
		CompanyID:  companyID,
		UserID:     u.ID,
		Permission: doc.UserPermOwn,
	}
	return cm.dbmodel.Save(uc, u)
}

func (cm *companyModel) DeleteCompanyUser(uid string, u *input.ReqUser) error {
	c := &doc.Company{
		ID: u.CompanyID,
	}
	pqi := c.GetPipeline(bson.M{"_id": u.CompanyID})
	err := cm.dbmodel.PipelineOne(c, pqi.GetPipeline())
	if err != nil {
		return rsrc.NewApiError(http.StatusNotFound, "company not found")
	}
	var delUCPid bson.ObjectId
	for _, cp := range c.CompanyPerm {
		if cp.UserID.Hex() == uid {
			delUCPid = cp.ID
			break
		}
	}
	if !delUCPid.Valid() {
		return rsrc.NewApiError(http.StatusNotFound, "user not found")
	}

	if len(c.CompanyPerm) == 1 {
		return rsrc.NewApiError(http.StatusForbidden, "company perm must have one record")
	}

	ucp := &doc.UserCompanyPerm{
		ID: delUCPid,
	}
	return cm.dbmodel.RemoveByID(ucp)
}

func (cm *companyModel) ChangeCompUserPerm(ucup input.PutCompanyUserPerm, u *input.ReqUser) error {
	coid, err := doc.GetObjectID(ucup.CompanyID)
	if err != nil {
		return rsrc.NewApiError(http.StatusBadRequest, "invalid compandy id")
	}
	c := &doc.Company{
		ID: coid,
	}
	pqi := c.GetPipeline(bson.M{"_id": coid})
	err = cm.dbmodel.PipelineOne(c, pqi.GetPipeline())
	if err != nil {
		return rsrc.NewApiError(http.StatusNotFound, "company not found")
	}
	var updateUCP *doc.UserCompanyPerm
	for _, cp := range c.CompanyPerm {
		if cp.UserID.Hex() == ucup.UserID {
			updateUCP = cp
			break
		}
	}
	if updateUCP == nil {
		return rsrc.NewApiError(http.StatusNotFound, "user not found")
	}

	var ownerUser []string
	// 檢查更改後是否沒Owner
	for _, cp := range c.CompanyPerm {
		if cp.Permission == doc.UserPermOwn {
			ownerUser = append(ownerUser, cp.ID.Hex())
		}
	}
	if util.IsStrInList(ucup.UserID, ownerUser...) && len(ownerUser) == 1 {
		return rsrc.NewApiError(http.StatusForbidden, "must have one owner")
	}
	updateUCP.Permission = ucup.Perm

	return cm.dbmodel.Update(updateUCP, u)
}

func (cm *companyModel) ChangeCompUserNote(pcun input.PutCompanyUserNote, u *input.ReqUser) error {
	coid, err := doc.GetObjectID(pcun.CompanyID)
	if err != nil {
		return rsrc.NewApiError(http.StatusBadRequest, "invalid compandy id")
	}
	c := &doc.Company{
		ID: coid,
	}
	pqi := c.GetPipeline(bson.M{"_id": coid})
	err = cm.dbmodel.PipelineOne(c, pqi.GetPipeline())
	if err != nil {
		return rsrc.NewApiError(http.StatusNotFound, "company not found")
	}
	var updateUCP *doc.UserCompanyPerm
	for _, cp := range c.CompanyPerm {
		if cp.UserID.Hex() == pcun.UserID {
			updateUCP = cp
			break
		}
	}
	if updateUCP == nil {
		return rsrc.NewApiError(http.StatusNotFound, "user not found")
	}

	var ownerUser []string
	// 檢查更改後是否沒Owner
	for _, cp := range c.CompanyPerm {
		if cp.Permission == doc.UserPermOwn {
			ownerUser = append(ownerUser, cp.ID.Hex())
		}
	}
	if util.IsStrInList(pcun.UserID, ownerUser...) && len(ownerUser) == 1 {
		return rsrc.NewApiError(http.StatusForbidden, "must have one owner")
	}
	updateUCP.Note = pcun.Note

	return cm.dbmodel.Update(updateUCP, u)
}

func (cm *companyModel) IsCompanyExist(unitCode string) bool {
	id, err := doc.GetCompanyId(unitCode, false)
	if err != nil {
		return false
	}
	c := &doc.Company{
		ID: id,
	}

	err = cm.dbmodel.FindByID(c)
	if err != nil {
		return false
	}
	return c.UnitCode == unitCode
}

func (cm *companyModel) ResendInvitation(mailServ mail.MailServ, id bson.ObjectId, email string) error {
	i := &doc.Invitation{
		ID: id,
	}
	err := cm.dbmodel.FindByID(i)
	if err != nil {
		return err
	}
	title := inviatationTitleMap[doc.InvitationTypeCompanyUser]
	return mailServ.Subject(title).Html(i.Html).PlaintText(i.Plaint).SendSingle("", email)
}

func (cm *companyModel) InviteUser(mailServ mail.MailServ, pcun input.CreateCompanyUserPerm, u *input.ReqUser) error {
	// 檢查公司是否存在
	c := &doc.Company{
		ID: pcun.CompanyID,
	}
	cm.dbmodel.FindByID(c)
	if !c.ID.Valid() {
		return rsrc.NewApiError(http.StatusNotFound, "company not exist")
	}
	// 檢查被邀請者是否已加入
	q := bson.M{"email": pcun.Email}
	uu := &doc.User{}
	cm.dbmodel.PipelineOne(uu, uu.GetPipeline(q).GetPipeline())
	if uu.ID.Valid() {
		for _, cp := range uu.CompanyPerm {
			if cp.CompanyID == pcun.CompanyID {
				return rsrc.NewApiError(http.StatusConflict, "user exist")
			}
		}
	}
	// 檢查被邀請者是否已被邀請
	i := &doc.Invitation{}
	q["typ"] = doc.InvitationTypeCompanyUser
	q["companyid"] = pcun.CompanyID
	cm.dbmodel.FindOne(i, q)
	if i.ID.Valid() {
		return rsrc.NewApiError(http.StatusConflict, "user has invited")
	}
	// 建立邀請
	slice := strings.Split(pcun.Email, "@")
	parameter := map[string]interface{}{
		"email":   pcun.Email,
		"company": pcun.CompanyID.Hex(),
		"perm":    pcun.Perm,
	}

	inviteID := bson.NewObjectId()

	token, err := GetInviteToken(inviteID)
	if err != nil {
		return err
	}
	data := struct {
		Name, Invitor, Company, URL string
	}{
		slice[0],
		u.GetName(),
		c.Name,
		fmt.Sprintf(pcun.ConfirmURL, token),
	}
	html, err := GetTplString(mailServ.GetHtmlTemplateFile("inviteComUser.html"), data)
	if err != nil {
		return err
	}
	plaint, err := GetTplString(mailServ.GetTextTemplateFile("inviteComUser.txt"), data)
	if err != nil {
		return err
	}

	i = &doc.Invitation{
		ID:        inviteID,
		CompanyID: pcun.CompanyID,
		Email:     pcun.Email,
		Typ:       doc.InvitationTypeCompanyUser,
		Parameter: parameter,
		Plaint:    plaint,
		Html:      html,
	}
	err = cm.dbmodel.Save(i, u)
	if err != nil {
		return err
	}

	title := inviatationTitleMap[doc.InvitationTypeCompanyUser]
	return mailServ.Subject(title).Html(html).PlaintText(plaint).SendSingle("", pcun.Email)
}

type inviteCompanyUserHandler struct {
	dbmodel *mgoDBModel
	log     *log.Logger
}

func NewInviteCompanyUserHandler(mgodb *mgo.Database) InvitationReponseHandler {
	mongo := GetMgoDBModel(mgodb)
	return &inviteCompanyUserHandler{dbmodel: mongo, log: mongo.log}
}

func (ih inviteCompanyUserHandler) InvitationCheck(email string, parameter map[string]interface{}) error {
	u := &doc.User{}
	ih.dbmodel.FindOne(u, bson.M{"email": email})
	if !u.ID.Valid() {
		return rsrc.NewApiError(http.StatusNotFound, "user not found")
	}
	return nil
}
func (ih inviteCompanyUserHandler) InvitationAccept(email string, parameter map[string]interface{}) error {
	u := &doc.User{}
	ih.dbmodel.FindOne(u, bson.M{"email": email})
	if !u.ID.Valid() {
		return rsrc.NewApiError(http.StatusNotFound, "user not found")
	}
	up := &doc.UserCompanyPerm{
		UserID:     u.ID,
		CompanyID:  bson.ObjectIdHex(parameter["company"].(string)),
		Permission: parameter["perm"].(string),
	}

	return ih.dbmodel.Save(up, u)
}
func (ih inviteCompanyUserHandler) InvitationDeny(email string, parameter map[string]interface{}) error {
	u := &doc.User{}
	ih.dbmodel.FindOne(u, bson.M{"email": email})
	if !u.ID.Valid() {
		return rsrc.NewApiError(http.StatusNotFound, "user not found")
	}
	return nil
}
