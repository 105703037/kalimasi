package model

import (
	"fmt"
	"kalimasi/doc"
	"kalimasi/rsrc/einvoice"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func Test_getInvoice(t *testing.T) {
	ns := numaxSync{
		conf: &einvoice.NumaxConf{
			Url: `https://ranking.numax.com.tw/einvoice/api/v1/invoice`,
		},
	}
	dns := &doc.NumaxSync{Company: doc.Company{}}
	dns.UnitCode = "28862112"
	dns.NumaxSync.Auth = `y+yhv/YX0aHeZ2JoTf1yo5NbD6/lpDk/6/Xf6m8==QJ7mjy`
	dns.NumaxSync.Token = `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJOdW1heCIsImlhdCI6MTYxNDIyMTM0OCwiZXhwIjoxOTI5NTgxMzQ4LCJuYmYiOjE2MTQyMjEzNDgsInN1YiI6ImVhY2NvdW50Lm51bWF4LmNvbS50dyIsImp0aSI6IjhmM2Q5ZmZhYWU2OGRhYjBiNDk4NTMwZDVkYzI0YTg4IiwiZGF0YSI6eyJ1aWQiOjQsInVuaWZvcm1ObyI6IjI4ODYyMTEyIiwiYXV0aCI6InkreWh2XC9ZWDBhSGVaMkpvVGYxeW81TmJENlwvbHBEa1wvNlwvWGY2bTg9PVFKN21qeSJ9fQ.JRdq5kSMldfvDnNVCFumNBl-xgDAlzyj81P3GEI0Czk`
	r := ns.getInvoice(dns, time.Date(2021, 2, 23, 0, 0, 0, 0, time.Now().Location()), numaxInvStatusSuccess)
	fmt.Println(r)
	assert.True(t, false)
}
