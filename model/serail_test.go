package model

import (
	"fmt"
	"testing"
	"time"

	"kalimasi/doc"
	"kalimasi/rsrc"

	"github.com/globalsign/mgo/bson"
	"github.com/stretchr/testify/assert"
)

type testSerial string

func (t testSerial) GetC() string {
	return "test1"
}

func (t testSerial) GetCompany() bson.ObjectId {
	return bson.ObjectIdHex("ffffffffffffffff00000000")
}

func (t testSerial) GetDate() time.Time {
	return time.Now()
}

func (t testSerial) GetSerialType() string {
	return doc.TypeSerialVN
}

func Test_GetSerialNumber(t *testing.T) {
	rsrc.InitConfByFile("../conf/dev/config.yml", "Asia/Taipei")
	di := rsrc.GetDI()
	sm := NewSerialModel(di.GetMongoByKey("key"))
	// r, err := sm.GetAndUpdateSerial(cid, 2020, 10, 2, time.Now().Location())
	r, err := sm.GetAndUpdateSerial(testSerial("test"))
	fmt.Println(r, err)
	assert.True(t, false)
}

func Test_Update(t *testing.T) {
	rsrc.InitConfByFile("../conf/dev/config.yml", "Asia/Taipei")
	di := rsrc.GetDI()
	sm := NewSerialModel(di.GetMongoByKey("key"))
	r, err := sm.GetAndUpdateSerial(testSerial("test"))
	fmt.Println(r, err)
	assert.True(t, false)
}
