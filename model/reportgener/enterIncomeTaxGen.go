package reportgener

import (
	"kalimasi/doc"
	"kalimasi/report"
	"kalimasi/util"
	"strconv"
	"time"

	"github.com/globalsign/mgo"

	"github.com/globalsign/mgo/bson"
)

type EnterIncomeTaxGen struct {
	ReceiptDao   *mgo.Iter
	RebateDao    *mgo.Iter
	IgnorAccCode []string
	StartDate    time.Time
	EndDate      time.Time
	CompanyID    bson.ObjectId
	CompanyName  string
	VATnumber    string
	TaxNumber    string
}

func (bbr *EnterIncomeTaxGen) GetFileName() string {
	if !bbr.CompanyID.Valid() {
		panic("must set companyID")
	}
	t := ""
	if bbr.StartDate.Month() != bbr.EndDate.Month() {
		t = util.StrAppend(bbr.StartDate.Format("200601"), "&", bbr.EndDate.Format("200601"))
	} else {
		t = bbr.StartDate.Format("200601")
	}

	return util.StrAppend(bbr.CompanyID.Hex(), "/", t, "-balanceBudget-", strconv.FormatInt(time.Now().Unix(), 16))
}

func (bbr *EnterIncomeTaxGen) IsSetting() bool {
	return true
}

func (bbr *EnterIncomeTaxGen) GetReportID() bson.ObjectId {
	if !bbr.CompanyID.Valid() {
		panic("must set companyID")
	}
	return doc.GetReportID(bbr.GetType(), bbr.CompanyID)
}

func (bbr *EnterIncomeTaxGen) GetData() string {
	return ""
}

func (bbr *EnterIncomeTaxGen) GetTitle() string {
	return ""
}

func (bbr *EnterIncomeTaxGen) GetType() string {
	return "enterIncomeTax"
}

func (bbr *EnterIncomeTaxGen) Setting(setting string) error {
	return nil
}

func (bbr *EnterIncomeTaxGen) GetReportObj(title string, font map[string]string) (report.ReportInter, error) {
	ri := &report.EnterIncome{}
	ri.SetVATnumber(bbr.VATnumber)
	ri.SetTaxNumber(bbr.TaxNumber)
	ri.SetCompanyName(bbr.CompanyName)
	for {
		r := &doc.Receipt{}
		if !bbr.ReceiptDao.Next(r) {
			break
		}
		if r.TaxInfo.Amount == 0 {
			continue
		}
		if r.Typ == doc.TypeReceiptInvoice {
			continue
		}

		for _, at := range r.CreditAccTerm {
			if util.IsStrInList(at.Code, bbr.IgnorAccCode...) {
				continue
			}
		}

		for _, at := range r.DebitAccTerm {
			if util.IsStrInList(at.Code, bbr.IgnorAccCode...) {
				continue
			}
		}
		ri.AddReceipt(r)
	}
	for {
		r := &doc.RebateJoinReceipt{}
		if !bbr.RebateDao.Next(r) {
			break
		}
		ri.AddRebate(r)
	}
	return ri, nil
}
