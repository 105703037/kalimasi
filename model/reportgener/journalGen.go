package reportgener

import (
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/report"
	"kalimasi/rsrc/log"
	"kalimasi/util"
	"strconv"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type GeneralJounalGenInter interface {
	GetReport() report.ReportInter
	GetFileName() string
}

type generalJounalGenImpl struct {
	companyID bson.ObjectId
	query     input.QueryReportByYearAndTimeRage

	db   *mgo.Database
	log  *log.Logger
	font map[string]string
}

func NewJounalGen(db *mgo.Database, q input.QueryReportByYearAndTimeRage, companyID bson.ObjectId, font map[string]string, log *log.Logger) GeneralJounalGenInter {
	return &generalJounalGenImpl{
		companyID: companyID,
		query:     q,

		db:   db,
		log:  log,
		font: font,
	}
}

func (gjg *generalJounalGenImpl) GetReport() report.ReportInter {
	c := doc.Company{}
	err := gjg.db.C(c.GetC()).FindId(gjg.companyID).One(&c)
	if err != nil {
		gjg.log.Err(err.Error())
		return nil
	}
	ds := report.BalanceBudgetStyle("default")
	r := &report.Journal{
		Title:  c.Name,
		Start:  gjg.query.GetStartTime(),
		End:    gjg.query.GetEndTime(),
		Create: time.Now(),
		Style:  &ds,
		Font:   gjg.font,
	}
	a := doc.Account{
		CompanyID: gjg.companyID,
	}
	var al []*doc.Account
	mgoQuery := gjg.query.GetMgoQuery()
	mgoQuery["companyid"] = gjg.companyID
	err = gjg.db.C(a.GetC()).Find(mgoQuery).All(&al)
	if err != nil {
		gjg.log.Err(err.Error())
		return nil
	}

	no := ""
	var item *report.JournalItem
	for _, aa := range al {
		if aa.VoucherNumber == "" {
			item = &report.JournalItem{
				Date: aa.DateTime,
				No:   aa.VoucherNumber,
			}
		}
		if no != aa.VoucherNumber {
			if item != nil {
				r.AddItem(item)
			}
			no = aa.VoucherNumber
			item = &report.JournalItem{
				Date: aa.DateTime,
				No:   aa.VoucherNumber,
			}
		}

		item.AddAcc(&report.JournalAcc{
			AccCode: aa.Code,
			AccName: aa.Name,
			Amount:  aa.Amount,
			Typ:     aa.Typ,
			Summary: aa.Summary,
		})
	}
	if item != nil {
		r.AddItem(item)
	}
	return r
}

func (gjg *generalJounalGenImpl) GetFileName() string {
	if !gjg.companyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(gjg.companyID.Hex(), "/", strconv.Itoa(gjg.query.GetYear()), "-jounal-", strconv.FormatInt(time.Now().Unix(), 16))
}
