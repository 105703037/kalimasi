package model

import (
	"fmt"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/model/reportgener"
	"kalimasi/rsrc/log"
	"kalimasi/util"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

// AccountModel is used to handle 會計帳目功能
type AccountModel struct {
	dbmodel *mgoDBModel
	log     *log.Logger

	mdb *mgo.Database
}

// GetAccountModel return an accountModel which passes mongodb parameters
// to generate MgoDbModel.
func GetAccountModel(mgodb *mgo.Database) *AccountModel {
	mongo := GetMgoDBModel(mgodb)
	return &AccountModel{dbmodel: mongo, log: mongo.log, mdb: mgodb}
}

func (am *AccountModel) GetFinalAccountByQueryYearAndTimeRange(companyID bson.ObjectId, qtc input.QueryReportByYearAndTimeRage) (*FinalAccountMap, error) {
	a := &doc.GroupAccount{
		Account: doc.Account{
			CompanyID: companyID,
		},
	}
	q := a.GetFinalPipeline(qtc.GetMgoQuery())
	i, err := am.dbmodel.PipelineAll(a, q.GetPipeline())
	if err != nil {
		return nil, err
	}
	fam := &FinalAccountMap{}
	if result, ok := i.([]*doc.GroupAccount); ok {
		for _, r := range result {
			fam.Add(r.ID.Code, r.Name, r.ID.Typ, r.Amount)
		}
	}
	return fam, nil

}

func (am *AccountModel) GetFinalAccount(companyID bson.ObjectId, year int) (*FinalAccountMap, error) {
	a := &doc.GroupAccount{
		Account: doc.Account{
			CompanyID: companyID,
		},
	}
	q := a.GetFinalPipeline(bson.M{"year": year})
	i, err := am.dbmodel.PipelineAll(a, q.GetPipeline())
	if err != nil {
		return nil, err
	}
	fam := &FinalAccountMap{}
	if result, ok := i.([]*doc.GroupAccount); ok {
		for _, r := range result {
			fam.Add(r.ID.Code, r.Name, r.ID.Typ, r.Amount)
		}
	}
	return fam, nil
}

type FinalAccountMap struct {
	m map[string]*doc.BudgetItem
}

// IsNil returns whether is struct is Nil.
func (fa *FinalAccountMap) IsNil() bool {
	return fa == nil
}

// GetBalaceSheetItem 帶入參數code(會計科目), 回傳預算項目.
func (fa *FinalAccountMap) GetBalaceSheetItem(code string) *doc.BudgetItem {
	if i, ok := fa.m[code]; ok {
		return &doc.BudgetItem{
			Code:   i.Code,
			Amount: i.Amount,
			Name:   i.Name,
			Desc:   i.Desc,
		}
	}
	return &doc.BudgetItem{}
}

func (fa *FinalAccountMap) GetBudgetItem(code string, typ string) *doc.BudgetItem {
	if i, ok := fa.m[code]; ok {
		if typ == doc.TypeAccIncome {
			return &doc.BudgetItem{
				Code:   i.Code,
				Amount: i.Amount * -1,
				Name:   i.Name,
				Desc:   i.Desc,
			}
		}
		return &doc.BudgetItem{
			Code:   i.Code,
			Amount: i.Amount,
			Name:   i.Name,
			Desc:   i.Desc,
		}
	}
	return &doc.BudgetItem{}
}

func (fa *FinalAccountMap) Add(code, name, typ string, amount int) {
	if fa.m == nil {
		fa.m = make(map[string]*doc.BudgetItem)
	}
	if typ == doc.TypeAccountCredit {
		amount = -1 * amount
	}

	if bb, ok := fa.m[code]; ok {
		bb.Amount += amount
	} else {
		fa.m[code] = &doc.BudgetItem{
			Code:   code,
			Name:   name,
			Amount: amount,
		}
	}
}

func (am *AccountModel) ModifyOpening(open *input.PutAccountOpen, u *input.ReqUser) error {
	or := &doc.OpeningAccount{ID: open.ID, CompanyID: u.CompanyID}
	err := am.dbmodel.FindByID(or)
	if err != nil {
		return err
	}
	var ops []txn.Op

	delAcc := &doc.Account{CompanyID: u.CompanyID}
	for _, d := range open.DeleteAccterm {
		delAcc.ID = d
		ops = append(ops, delAcc.GetDelTxnOp())
	}

	or.DebitAccTerm = or.DebitAccTerm[:0]
	or.CreditAccTerm = or.CreditAccTerm[:0]

	dt := or.GetDate()

	for _, d := range open.DebitAccTerm {
		acc := d.GetAccount(dt, doc.TypeAccountDebit, nil, or.CompanyID, or.VoucherNumber, true)
		if acc.ID.Valid() {
			ops = append(ops, acc.GetUpdateTxnOp(
				bson.D{
					{Name: "code", Value: acc.Code},
					{Name: "name", Value: acc.Name},
					{Name: "amount", Value: acc.Amount},
					{Name: "summary", Value: acc.Summary},
					{Name: "datetime", Value: dt},
					{Name: "year", Value: dt.Year() - 1911},
					{Name: "typ", Value: doc.TypeAccountDebit},
				}))
		} else {
			ops = append(ops, acc.GetSaveTxnOp(u))
		}
		or.DebitAccTerm = append(or.DebitAccTerm, acc.GetMiniAccount())
	}

	for _, d := range open.CreditAccTerm {
		acc := d.GetAccount(dt, doc.TypeAccountCredit, nil, u.CompanyID, or.VoucherNumber, true)
		if acc.ID.Valid() {
			ops = append(ops, acc.GetUpdateTxnOp(
				bson.D{
					{Name: "code", Value: acc.Code},
					{Name: "name", Value: acc.Name},
					{Name: "amount", Value: acc.Amount},
					{Name: "summary", Value: acc.Summary},
					{Name: "datetime", Value: dt},
					{Name: "year", Value: dt.Year() - 1911},
					{Name: "typ", Value: doc.TypeAccountCredit},
				}))
		} else {
			ops = append(ops, acc.GetSaveTxnOp(u))
		}

		or.CreditAccTerm = append(or.CreditAccTerm, acc.GetMiniAccount())
	}

	ops = append(ops, or.GetUpdateTxnOp(
		bson.D{
			{Name: "datetime", Value: dt},
			{Name: "year", Value: dt.Year() - 1911},
			{Name: "debitaccterm", Value: or.DebitAccTerm},
			{Name: "creditaccterm", Value: or.CreditAccTerm},
		}))

	err = am.dbmodel.RunTxn(ops)
	if err != nil {
		return err
	}
	return am.dbmodel.addDocLog(or, u, doc.ActUpdate)
}

func (am *AccountModel) Opening(open *input.CreateAccountOpen, u *input.ReqUser) error {
	bs := &doc.OpeningAccount{
		Year: open.Year,
	}
	var ops []txn.Op
	bs.SetCompany(u.GetCompany())

	dt := bs.GetDate()
	bs.VoucherNumber = util.GetVoucherNumber(dt, 0)

	for _, d := range open.DebitAccTerm {
		acc := d.GetAccount(dt, doc.TypeAccountDebit, nil, u.GetCompany(), bs.VoucherNumber, true)
		ops = append(ops, acc.GetSaveTxnOp(u))
		bs.DebitAccTerm = append(bs.DebitAccTerm, acc.GetMiniAccount())
	}

	for _, c := range open.CreditAccTerm {
		acc := c.GetAccount(dt, doc.TypeAccountCredit, nil, u.GetCompany(), bs.VoucherNumber, true)
		ops = append(ops, acc.GetSaveTxnOp(u))
		bs.CreditAccTerm = append(bs.CreditAccTerm, acc.GetMiniAccount())
	}

	ops = append(ops, bs.GetSaveTxnOp(u))
	return am.dbmodel.RunTxn(ops)
}

// 關帳會依資產負債表的會科所有的餘額並轉入隔年
func (am *AccountModel) Closing(company bson.ObjectId, year int) error {
	// 查詢上一期的結算資料

	// 結算當期的資產負債表
	fa, err := am.GetFinalAccount(company, year)
	if err != nil {
		return err
	}
	bb := &reportgener.BalanceSheet{
		CompanyID: company,
		Year:      year,
	}
	rm := GetReportModel(am.mdb)
	if err = rm.LoadSetting(bb); err != nil {
		return err
	}
	const empty = ""
	for _, asset := range bb.Assets {
		for _, sa := range asset.Sub {
			bb := fa.GetBalaceSheetItem(sa.AccCode)
			if bb.Code != empty {
				am.log.Debug(fmt.Sprintln(bb.Code, bb.Name, bb.Amount))
			}
		}
	}

	for _, liab := range bb.Liab {
		for _, ls := range liab.Sub {
			bb := fa.GetBalaceSheetItem(ls.AccCode)
			if bb.Code != empty {
				am.log.Debug(fmt.Sprintln(bb.Code, bb.Name, bb.Amount))
			}
		}
	}
	return nil
}

// func (am *AccountModel) GetTaxFileMap(companyID bson.ObjectId, year int) (*TaxFileMap, error) {
// 	a := &doc.GroupAccount{
// 		Account: doc.Account{
// 			CompanyID: companyID,
// 		},
// 	}
// 	q := a.GetFinalPipeline(bson.M{"year": year})
// 	i, err := am.dbmodel.PipelineAll(a, q.GetPipeline())
// 	if err != nil {
// 		return nil, err
// 	}
// 	fam := &TaxFileMap{}
// 	if result, ok := i.([]*doc.GroupAccount); ok {
// 		for _, r := range result {
// 			fam.Add(r.ID.Code, r.Name, r.ID.Typ, r.Amount)
// 		}
// 	}
// 	return fam, nil
// }

// type TaxFileMap struct {
// 	income  map[string]*doc.BudgetItem
// 	outcome map[string]*doc.BudgetItem
// }

// // IsNil returns whether is struct is Nil.
// func (fa *TaxFileMap) IsNil() bool {
// 	return fa == nil
// }

// func (fa *TaxFileMap) GetBudgetItem(code string, typ string) *doc.BudgetItem {
// 	if typ == doc.TypeAccIncome {
// 		if i, ok := fa.income[code]; ok {
// 			return &doc.BudgetItem{
// 				Code:   i.Code,
// 				Amount: i.Amount,
// 				Name:   i.Name,
// 				Desc:   i.Desc,
// 			}
// 		}
// 		return &doc.BudgetItem{}
// 	}

// 	if i, ok := fa.outcome[code]; ok {
// 		return &doc.BudgetItem{
// 			Code:   i.Code,
// 			Amount: i.Amount,
// 			Name:   i.Name,
// 			Desc:   i.Desc,
// 		}
// 	}
// 	return &doc.BudgetItem{}
// }

// func (fa *TaxFileMap) Add(code, name, typ string, amount int) {
// 	if fa.income == nil {
// 		fa.income = make(map[string]*doc.BudgetItem)
// 	}
// 	if fa.outcome == nil {
// 		fa.outcome = make(map[string]*doc.BudgetItem)
// 	}
// 	fmt.Println(code, name, typ, amount)
// 	if typ == doc.TypeAccountCredit {
// 		// 貸方
// 		if bb, ok := fa.income[code]; ok {
// 			bb.Amount += amount
// 		} else {
// 			fa.income[code] = &doc.BudgetItem{
// 				Code:   code,
// 				Name:   name,
// 				Amount: amount,
// 			}
// 		}
// 	} else if typ == doc.TypeAccountDebit {
// 		// 借方
// 		if bb, ok := fa.outcome[code]; ok {
// 			bb.Amount += amount
// 		} else {
// 			fa.outcome[code] = &doc.BudgetItem{
// 				Code:   code,
// 				Name:   name,
// 				Amount: amount,
// 			}
// 		}
// 	}

// }
