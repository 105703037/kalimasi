package model

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"kalimasi/doc"
	"kalimasi/rsrc/einvoice"
	"kalimasi/rsrc/log"
	"net/http"
	"time"

	"github.com/globalsign/mgo/bson"
)

const (
	numaxInvStatusSuccess = "success" // 發票開立成功
)

func NewNumaxSync(conf *einvoice.NumaxConf, dbmodel *mgoDBModel, log *log.Logger) *numaxSync {
	return &numaxSync{
		conf:    conf,
		dbmodel: dbmodel,
		log:     log,
	}
}

type numaxSync struct {
	conf    *einvoice.NumaxConf
	dbmodel *mgoDBModel
	log     *log.Logger
}

// 指定公司同步資料
func (ns *numaxSync) SyncByCompany(cid bson.ObjectId, d time.Time) error {
	c := &doc.NumaxSync{
		Company: doc.Company{ID: cid},
	}
	err := ns.dbmodel.FindByID(c)
	if err != nil {
		return err
	}
	if c.NumaxSync.Token == "" {
		return errors.New("sync info not set")
	}
	einvoice := ns.getInvoice(c, d, numaxInvStatusSuccess)
	if einvoice == nil {
		return nil
	}
	for _, e := range einvoice {
		err = ns.dbmodel.Upsert(e, nil)
		if err != nil {
			ns.log.Warn(err.Error())
		}
	}
	c.NumaxSync.SyncTime = time.Now()
	ns.dbmodel.Update(c, nil)
	return nil
}

// 依日期同步所有公司資料
func (ns *numaxSync) SyncByDate(d time.Time) error {
	coms := ns.getSyncCompany()
	var einvoice []*doc.EInvoice
	var err error
	var failEinvoice []*doc.EInvoice
	for _, c := range coms {
		// 成功開立發票同步
		einvoice = ns.getInvoice(c, d, numaxInvStatusSuccess)
		if einvoice == nil {
			continue
		}
		for _, e := range einvoice {
			err = ns.dbmodel.Save(e, nil)
			if err != nil {
				ns.log.Warn(err.Error())
				failEinvoice = append(failEinvoice, e)
			}
		}
		c.NumaxSync.SyncTime = time.Now()
		ns.dbmodel.Update(c, nil)
	}
	return nil
}

func (ns *numaxSync) getSyncCompany() []*doc.NumaxSync {
	c := &doc.NumaxSync{}
	result, err := ns.dbmodel.Find(c, bson.M{"numaxsync": bson.M{"$exists": true}}, 0, 0)
	if err != nil {
		ns.log.Warn("syncCompany not found: " + err.Error())
		return nil
	}
	return result.([]*doc.NumaxSync)
}

func (ns *numaxSync) getInvoice(c *doc.NumaxSync, d time.Time, status string) []*doc.EInvoice {
	req, err := http.NewRequest("GET", ns.conf.Url, nil)
	if err != nil {
		ns.log.Err(err.Error())
		return nil
	}
	req.Header.Set("Token", c.NumaxSync.Token)
	req.Header.Set("Authorization", c.NumaxSync.Auth)
	q := req.URL.Query()
	q.Add("date", d.Format("2006-01-02"))
	q.Add("seller", c.NumaxSync.Seller)
	q.Add("status", status)
	ns.log.Debug(fmt.Sprintln(ns.conf.Url, c.NumaxSync.Token, c.NumaxSync.Auth, d.Format("2006-01-02"), c.UnitCode, status))
	req.URL.RawQuery = q.Encode()

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		ns.log.Warn(err.Error())
		return nil
	}

	defer resp.Body.Close()
	respBody, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		ns.log.Warn(string(respBody))
		return nil
	}
	nr := &numaxResp{}
	respBody = bytes.TrimPrefix(respBody, []byte("\xef\xbb\xbf"))
	err = json.Unmarshal(respBody, nr)
	if err != nil {
		ns.log.Warn(err.Error())
		return nil
	}
	var invoiceList []*doc.EInvoice
	for _, n := range nr.Result {
		invoiceList = append(invoiceList, n.toEInvoice(c.ID))
	}
	return invoiceList
}

type numaxResp struct {
	Status string
	Result map[string]numaxInvoice
}

type numaxInvoice struct {
	BuyerUniformNo string    `json:"buyerUniformNo"`
	InvoiceNo      string    `json:"invoiceNo"`
	Date           time.Time `json:"invoiceDate"`
	Amount         int       `json:"invoiceAmount"`
	TaxType        string    `json:"taxType"`
	Tax            int       `json:"tax"`
	ImageURL       string    `json:"imageUrl"`
	ProductList    []struct {
		Name      string
		Number    int
		Unit      string
		UnitPrice float64 `json:"unitPrice"`
		Amount    float64
	} `json:"productList"`
}

func (ni *numaxInvoice) toEInvoice(cid bson.ObjectId) *doc.EInvoice {
	var taxType string
	switch ni.TaxType {
	case "tax":
		taxType = doc.TypeTaxTable
	case "zero":
		taxType = doc.TypeTaxZero
	case "free":
		taxType = doc.TypeTaxFree
	case "special":
		taxType = doc.TypeTaxSepecial
	default:
		panic("not support tax type: " + ni.TaxType)
	}
	ei := &doc.EInvoice{
		CompanyID:     cid,
		Source:        "numax",
		DateTime:      ni.Date,
		VoucherNumber: ni.InvoiceNo,
		VatNumber:     ni.BuyerUniformNo,
		Amount:        ni.Amount,
		Tax:           ni.Tax,
		TaxType:       taxType,
		ImgUrl:        ni.ImageURL,
	}

	for _, p := range ni.ProductList {
		ei.AddDetail(p.Name, p.Number, p.Unit, p.UnitPrice, p.Amount)
	}

	return ei
}
