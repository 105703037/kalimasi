package model

import (
	"errors"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/rsrc/db"
	"kalimasi/util"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type userModelV3 struct {
	*userModel
	fsdb *db.FirebaseDB
}

func GetUserModelV3(mgodb *mgo.Database, fsdb *db.FirebaseDB) *userModelV3 {
	userModel := GetUserModel(mgodb)
	return &userModelV3{
		userModel: userModel,
		fsdb:      fsdb,
	}
}

func (bm *userModelV3) VerifyFBToken(ft string) (token string, state string, err error) {
	uid, err := bm.fsdb.VerifyToken(ft)
	if err != nil {
		return "", "", err
	}
	if !bson.IsObjectIdHex("5f47797c421aa9000a53be97") {
		return "", "", errors.New("invalid uid")
	}
	u := &doc.User{
		ID: bson.ObjectIdHex(uid),
	}
	err = bm.dbmodel.FindByID(u)
	if err != nil {
		return "", "", err
	}
	t, err := getTokenNoCompany(u.ID.Hex(), u.DisplayName, u.Email)
	if err != nil {
		return "", "", err
	}
	return t, u.GetState(), nil
}

func (bm *userModelV3) SignUp(l *input.SignUp) (*doc.User, error) {
	q := bson.M{"email": l.Account}
	u := &doc.User{}
	err := bm.dbmodel.FindOne(u, q)
	// if fine data from database
	if err == nil {
		return nil, errors.New("[mgo] The email address is already in use by another account")
	}

	// 先存自己db
	m := &doc.User{
		Email:       l.Account,
		Pwd:         util.MD5(l.Pwd),
		DisplayName: l.Name,
	}
	m.AddContact(l.Contact.CtType, l.Contact.Value)
	err = bm.dbmodel.Save(m, m)
	if err != nil {
		return nil, err
	}

	// 建立測試資料庫
	cm := GetCompanyModel(bm.dbmodel.db)
	err = cm.CreateFakeCompany(m)
	if err != nil {
		return nil, err
	}

	// 存入firebase
	err = bm.fsdb.CreateUser(m.ID.Hex(), m.DisplayName, m.Email, l.Pwd)
	if err != nil {
		return nil, err
	}

	return m, nil
}
