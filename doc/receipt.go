/*
 * 憑證管理
 */
package doc

import (
	"kalimasi/util"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	receiptC  = "receipt"
	einvoiceC = "einvoice"

	TypeReceiptElectronic = "electronic" // 電子發票
	TypeReceiptTriplicate = "triplicate" // 三聯發票
	TypeReceiptDuplicate  = "duplicate"  // 二聯發票
	TypeReceiptInvoice    = "invoice"    // 憑證
	TypeReceiptCustoms    = "customs"    // 海關

	TypeTaxTable    = "tax"
	TypeTaxFree     = "free"    // 免稅
	TypeTaxZero     = "zero"    // 零稅
	TypeTaxNone     = "none"    // 無法扣抵
	TypeTaxSepecial = "special" // 特種稅率

	TypeAccIncome = "income"
	TypeAccExpand = "expenses"
)

type Tax struct {
	Typ       string
	Amount    int // 使用者輸入稅額
	SysAmount int // 系統計算稅額
	Rate      int
}

type Receipt struct {
	ID              bson.ObjectId `bson:"_id"`
	DateTime        time.Time
	VoucherNumber   string
	Year            int
	Typ             string // 電子發票 / 三聯 / 二聯 / 憑證 / 海關
	TypeAcc         string // 進項 / 銷項
	NoVATAndNumber  bool   // 無發票號碼及統一編號
	Number          string // 發票號碼
	VATnumber       string // 統一編號
	Amount          int
	IsCollect       bool // 是否為匯總
	CollectQuantity int
	IsFixedAsset    bool

	Pictures []string // 上傳圖片清單
	TaxInfo  Tax

	CompanyID bson.ObjectId
	BudgetId  *bson.ObjectId

	DebitAccTerm  []*MiniAccount
	CreditAccTerm []*MiniAccount

	CommonDoc `bson:"meta"`
	AuditID
	LookUpAudit `bson:"lookupAudit,omitempty"`
}

func (r *Receipt) GetDebitAccTerm(id bson.ObjectId) *MiniAccount {
	for _, d := range r.DebitAccTerm {
		if d.ID == id {
			return d
		}
	}
	return nil
}

func (r *Receipt) GetCreditAccTerm(id bson.ObjectId) *MiniAccount {
	for _, d := range r.CreditAccTerm {
		if d.ID == id {
			return d
		}
	}
	return nil
}

func (r *Receipt) GetDoc() interface{} {
	if !r.ID.Valid() {
		r.ID = r.newID()
	}
	r.Year = r.DateTime.Year() - 1911
	return r
}

func (r *Receipt) GetID() bson.ObjectId {
	return r.ID
}

func (r *Receipt) GetC() string {
	if !r.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(r.CompanyID.Hex(), receiptC)
}

func (u *Receipt) SetCompany(c bson.ObjectId) {
	u.CompanyID = c
}

func (u *Receipt) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *Receipt) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *Receipt) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}

func (r *Receipt) GetMongoIndexes() []mgo.Index {
	return []mgo.Index{
		{
			Key:        []string{"companyid", "year", "typeacc"},
			Background: true, // can be used while index is being built
		},
	}
}

func (u *Receipt) GetPipeline(q bson.M) PipelineQryInter {
	a := &Audit{CompanyID: u.CompanyID}
	return &CommonPipeline{
		q: q,
		pipeline: []bson.M{
			{"$match": q},
			{
				"$lookup": bson.M{
					"from":         a.GetC(),
					"localField":   "auditid",
					"foreignField": "_id",
					"as":           "lookupAudit",
				},
			},
		},
	}
}

// serialDoc interface
func (u *Receipt) GetCompany() bson.ObjectId {
	return u.CompanyID
}

func (u *Receipt) GetDate() time.Time {
	return u.DateTime
}

func (u *Receipt) GetSerialType() string {
	return TypeSerialVN
}

type EInvoice struct {
	ID        bson.ObjectId `bson:"_id"`
	CompanyID bson.ObjectId
	DateTime  time.Time
	// 發票號碼
	VoucherNumber string
	// 統一編號
	VatNumber string
	Source    string
	Amount    int
	ReceiptID *bson.ObjectId
	Tax       int
	TaxType   string
	Detail    []*eInvDetail
	ImgUrl    string

	CommonDoc `bson:"meta"`
	AuditID
	LookUpAudit `bson:"lookupAudit,omitempty"`
}

type eInvDetail struct {
	Name      string  `json:"name"`      // 商品名稱
	Number    int     `json:"number"`    // 商品數量
	Unit      string  `json:"unit"`      // 商品數量
	UnitPrice float64 `json:"unitPrice"` // 商品單價
	Amount    float64 `json:"amount"`    // 商品小計
}

func (r *EInvoice) AddDetail(name string, number int, unit string, unitPrice, amount float64) {
	r.Detail = append(r.Detail, &eInvDetail{
		Name:      name,
		Number:    number,
		Unit:      unit,
		UnitPrice: unitPrice,
		Amount:    amount,
	})
}

func (r *EInvoice) GetC() string {
	if !r.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(r.CompanyID.Hex(), einvoiceC)
}

func (u *EInvoice) GetDoc() interface{} {
	if !u.ID.Valid() {
		u.ID = u.newID()
	}
	return u
}

func (u *EInvoice) GetUpdateField() bson.M {
	return bson.M{
		"receiptid": u.ReceiptID,
	}
}

func (u *EInvoice) GetID() bson.ObjectId {
	if !u.ID.Valid() {
		u.ID = u.newID()
	}
	return u.ID
}

func (d *EInvoice) newID() bson.ObjectId {
	numByte := []byte(d.VoucherNumber)
	numLen := len(numByte)
	if numLen > 12 {
		panic("voucherNumber error")
	}
	var b [12]byte
	for i := 0; i < numLen; i++ {
		b[i] = numByte[i]
	}
	return bson.ObjectId(b[:])
}

func (u *EInvoice) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *EInvoice) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *EInvoice) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}
