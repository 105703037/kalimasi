package doc

import (
	"encoding/binary"
	"errors"
	"kalimasi/util"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	serialNumberC = "serialNumber"
	serialRecordC = "serialRecord"

	TypeSerialVN = "voucherNumber"
)

type SerialRecord struct {
	ID        bson.ObjectId `bson:"_id"`
	Date      time.Time
	CompanyID bson.ObjectId
	Key       string
	C         string
	Serial    int64
	CommonDoc `bson:"meta"`
}

func (sn *SerialRecord) GetDoc() interface{} {
	if !sn.ID.Valid() {
		sn.ID = sn.newID()
	}

	return sn
}

func (sn *SerialRecord) GetID() bson.ObjectId {
	if !sn.ID.Valid() {
		sn.ID = sn.newID()
	}
	return sn.ID
}

func (sn *SerialRecord) GetC() string {
	if !sn.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(sn.CompanyID.Hex(), serialRecordC)
}

func (sn *SerialRecord) SetCompany(c bson.ObjectId) {
	sn.CompanyID = c
}

func (sn *SerialRecord) GetSaveTxnOp(lu LogUser) txn.Op {
	return sn.CommonDoc.getSaveTxnOp(sn, lu)
}

func (sn *SerialRecord) GetUpdateTxnOp(data bson.D) txn.Op {
	return sn.CommonDoc.getUpdateTxnOp(sn, data)
}

func (sn *SerialRecord) GetDelTxnOp() txn.Op {
	return sn.CommonDoc.getDelTxnOp(sn)
}

func (sn *SerialRecord) GetUpdateField() bson.M {
	return bson.M{}
}

func (sn *SerialRecord) GetMongoIndexes() []mgo.Index {
	return []mgo.Index{
		{
			Key:        []string{"date", "key", "c"},
			Background: true, // can be used while index is being built
		},
	}
}

type SerialNumber struct {
	ID        bson.ObjectId `bson:"_id"`
	Date      time.Time
	CompanyID bson.ObjectId

	NumberMap map[string]int64

	CommonDoc `bson:"meta"`
}

func (sn *SerialNumber) GetSerialNo(key string) int64 {
	if s, ok := sn.NumberMap[key]; ok {
		return s + 1
	}
	return 1
}

func GetSerialNumberId(companyID bson.ObjectId, dateTime time.Time) (bson.ObjectId, error) {
	if !companyID.Valid() {
		return "", errors.New("missing companyID")
	}
	var b [4]byte
	binary.BigEndian.PutUint32(b[:], uint32(dateTime.Unix()))

	byteOid := []byte(companyID)
	for i := 0; i < 4; i++ {
		byteOid[i] = b[i]
	}
	return bson.ObjectId(byteOid[:]), nil
}

func (sn *SerialNumber) newID() bson.ObjectId {
	id, err := GetSerialNumberId(sn.CompanyID, sn.Date)
	if err != nil {
		panic(err)
	}
	return id
}

func (sn *SerialNumber) GetDoc() interface{} {
	if !sn.ID.Valid() {
		sn.ID = sn.newID()
	}

	return sn
}

func (sn *SerialNumber) GetID() bson.ObjectId {
	if !sn.ID.Valid() {
		sn.ID = sn.newID()
	}
	return sn.ID
}

func (sn *SerialNumber) GetC() string {
	if !sn.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(sn.CompanyID.Hex(), serialNumberC)
}

func (sn *SerialNumber) SetCompany(c bson.ObjectId) {
	sn.CompanyID = c
}

func (sn *SerialNumber) GetSaveTxnOp(lu LogUser) txn.Op {
	return sn.CommonDoc.getSaveTxnOp(sn, lu)
}

func (sn *SerialNumber) GetUpdateTxnOp(data bson.D) txn.Op {
	return sn.CommonDoc.getUpdateTxnOp(sn, data)
}

func (sn *SerialNumber) GetDelTxnOp() txn.Op {
	return sn.CommonDoc.getDelTxnOp(sn)
}

func (sn *SerialNumber) GetUpdateField() bson.M {
	return bson.M{
		"numbermap": sn.NumberMap,
		"companyid": sn.CompanyID,
	}
}

func (sn *SerialNumber) GetMongoIndexes() []mgo.Index {
	return []mgo.Index{
		{
			Key:        []string{"companyid", "year", "typeacc"},
			Background: true, // can be used while index is being built
		},
	}
}
