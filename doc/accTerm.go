package doc

import (
	"errors"
	"strings"

	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

type AccTerm struct {
	Name   string `json:"name"`
	Code   string `json:"code"`
	Desc   string `json:"desc"`
	Enable bool   `json:"enable"`
}

type AccTermGroup struct {
	Name   string     `json:"name"`
	Code   string     `json:"code"`
	Desc   string     `json:"desc"`
	Enable bool       `json:"enable"`
	Sub    []*AccTerm `json:"sub"`
}

func (at *AccTermGroup) AddSub(name string, code string) error {
	for _, t := range at.Sub {
		if t.Code == code {
			return errors.New("duplicate accounting term code")
		}
	}
	at.Sub = append(at.Sub, &AccTerm{Name: name, Code: code})
	return nil
}

type AccTermList struct {
	ID        bson.ObjectId `bson:"_id"`
	Typ       string
	CompanyID bson.ObjectId
	Terms     []*AccTermGroup
	Enable    bool
	CommonDoc `bson:"meta"`
}

const (
	accTermC = "accTerm"

	TypeAccTermAssets   = "assets"   //資產 Assets
	TypeAccTermDebt     = "debt"     //負債科目 Liabilities
	TypeAccTermFund     = "fund"     //股東權益 Stockholders' Equity
	TypeAccTermIncome   = "income"   //營業收入 Operating Income
	TypeAccTermCost     = "cost"     //營業支出(成本) Operating Costs
	TypeAccTermExpenses = "expenses" //營業利潤(費用) Operating Expenses
)

var (
	TypeMapCode = map[string]int{
		TypeAccTermAssets:   1,
		TypeAccTermDebt:     2,
		TypeAccTermFund:     3,
		TypeAccTermIncome:   4,
		TypeAccTermCost:     5,
		TypeAccTermExpenses: 6,
	}
)

func ConvertDefaultAccTermId(id bson.ObjectId) bson.ObjectId {
	var b [12]byte
	l := len(b)
	for i := 0; i < l; i++ {
		b[i] = 255
	}
	tb := []byte(string(id)[8:9])
	b[8] = tb[0]
	return bson.ObjectId(b[:])
}

func GetDefaultAccTermId(typ string) (bson.ObjectId, error) {
	var b [12]byte
	l := len(b)
	for i := 0; i < l; i++ {
		b[i] = 255
	}

	code := TypeMapCode[typ]
	b[8] = byte(code)
	return bson.ObjectId(b[:]), nil
}

func GetAccTermId(companyId bson.ObjectId, typ string) (bson.ObjectId, error) {
	if !companyId.Valid() {
		return "", errors.New("companyid is invalid")
	}
	b := []byte(companyId)
	code := TypeMapCode[typ]
	b[8] = byte(code)
	return bson.ObjectId(b[:]), nil
}

func (w *AccTermList) GetID() bson.ObjectId {
	if w.ID.Valid() {
		return w.ID
	}
	id, err := w.getID()
	if err != nil {
		return ""
	}
	w.ID = id
	return w.ID
}

func (d *AccTermList) getID() (bson.ObjectId, error) {
	return GetAccTermId(d.CompanyID, d.Typ)
}

func (u *AccTermList) SetCompany(c bson.ObjectId) {
	u.CompanyID = c
}

func (w *AccTermList) GetC() string {
	return accTermC
}

func (at *AccTermList) GetDoc() interface{} {
	if !at.ID.Valid() {
		id, err := at.getID()
		if err != nil {
			return nil
		}
		at.ID = id

	}
	return at
}

func (w *AccTermList) BeforeSave() error {
	if id, err := w.getID(); err != nil {
		w.ID = id
	} else {
		return err
	}
	return nil
}

func (u *AccTermList) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *AccTermList) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *AccTermList) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}

func (u *AccTermList) GetAccTerm(code string) *AccTerm {
	if u == nil {
		return nil
	}
	for _, g := range u.Terms {
		if !strings.HasPrefix(code, g.Code) {
			continue
		}
		if code == g.Code {
			return &AccTerm{
				Name: g.Name,
				Code: g.Code,
			}
		}
		for _, sub := range g.Sub {
			if sub.Code == code {
				return &AccTerm{
					Name: sub.Name,
					Code: sub.Code,
				}
			}
		}
	}
	return nil
}

func (u *AccTermList) GetMapAmount() map[string]int {
	result := make(map[string]int)
	for _, i := range u.Terms {
		result[i.Code] = 0
		for _, j := range i.Sub {
			result[j.Code] = 0
		}
	}
	return result
}

func (u *AccTermList) GetAccTermAry() []*AccTerm {
	var result []*AccTerm
	for _, i := range u.Terms {
		result = append(result, &AccTerm{
			Name: i.Name,
			Code: i.Code,
		})
		for _, j := range i.Sub {
			result = append(result, &AccTerm{
				Name: j.Name,
				Code: j.Code,
			})
		}
	}
	return result
}

func (u *AccTermList) GetUpdateField() bson.M {
	return bson.M{
		"terms":  u.Terms,
		"enable": u.Enable,
	}
}
