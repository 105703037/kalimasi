package doc

import (
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	invitationC = "invitation"

	// 邀請人進入公司管理帳務
	InvitationTypeCompanyUser = "inviteComUser"

	InvitationReponseAccept = "accept"
	InvitationReponseDeny   = "deny"
	InvitationReponseCheck  = "check"

	// 邀請人成為公司會計顧問
	InvitationTypeConsult = "inviteConsult"
)

type Invitation struct {
	ID        bson.ObjectId `bson:"_id"`
	CompanyID bson.ObjectId
	Email     string
	Typ       string // 邀請信類型
	Parameter map[string]interface{}
	Plaint    string // 信件內容
	Html      string
	SendTime  time.Time

	CommonDoc `bson:"meta"`
}

func (u *Invitation) GetID() bson.ObjectId {
	if !u.ID.Valid() {
		u.ID = u.newID()
	}
	return u.ID
}

func (u *Invitation) GetDoc() interface{} {
	if !u.ID.Valid() {
		u.ID = u.newID()
	}
	return u
}

func (u *Invitation) GetC() string {
	return invitationC
}

func (u *Invitation) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *Invitation) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *Invitation) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}
