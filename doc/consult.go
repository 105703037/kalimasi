package doc

import (
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	consultC    = "consult"
	inviteMailC = "inviteMail"
)

type ContactStruct struct {
	CtType string `json:"ctType"`
	Value  string `json:"value"`
}

type Consult struct {
	ID        bson.ObjectId `bson:"_id"`
	Account   string        `json:"account" bson:"account"`
	Pwd       string        `json:"pwd" bson:"pwd"`
	Name      string        `json:"name" bson:"name"`
	Contact   ContactStruct `json:"contact" bson:"contact"`
	License   string        `json:"license" bson:"license"`
	CommonDoc `bson:"meta"`
}

func (c *Consult) newID() bson.ObjectId {
	id := bson.NewObjectId()
	return id
}

func (c *Consult) GetC() string {
	return consultC
}

func (c *Consult) GetDoc() interface{} {
	if !c.ID.Valid() {
		c.ID = c.newID()
	}
	return c
}

func (c *Consult) GetID() bson.ObjectId {
	return c.ID
}

func (c *Consult) GetUpdateField() bson.M {
	return bson.M{
		"account": c.Account,
		"pwd":     c.Pwd,
		"name":    c.Name,
		"contact": c.Contact,
		"license": c.License,
	}
}

func (c *Consult) GetSaveTxnOp(lu LogUser) txn.Op {
	return c.CommonDoc.getSaveTxnOp(c, lu)
}

func (c *Consult) GetUpdateTxnOp(data bson.D) txn.Op {
	return c.CommonDoc.getUpdateTxnOp(c, data)
}

func (c *Consult) GetDelTxnOp() txn.Op {
	return c.CommonDoc.getDelTxnOp(c)
}

type InviteMail struct {
	ID        bson.ObjectId `bson:"_id"`
	Mail      string        `json:"mail" bson:"mail"`
	CommonDoc `bson:"meta"`
}

func (i *InviteMail) newID() bson.ObjectId {
	id := bson.NewObjectId()
	return id
}

func (i *InviteMail) GetC() string {
	return inviteMailC
}

func (i *InviteMail) GetDoc() interface{} {
	if !i.ID.Valid() {
		i.ID = i.newID()
	}
	return i
}

func (i *InviteMail) GetID() bson.ObjectId {
	return i.ID
}

func (i *InviteMail) GetUpdateField() bson.M {
	return bson.M{
		"mail": i.Mail,
	}
}

func (i *InviteMail) GetSaveTxnOp(lu LogUser) txn.Op {
	return i.CommonDoc.getSaveTxnOp(i, lu)
}

func (i *InviteMail) GetUpdateTxnOp(data bson.D) txn.Op {
	return i.CommonDoc.getUpdateTxnOp(i, data)
}

func (i *InviteMail) GetDelTxnOp() txn.Op {
	return i.CommonDoc.getDelTxnOp(i)
}
