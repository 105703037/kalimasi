package doc

import (
	"encoding/binary"
	"fmt"
	"testing"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/stretchr/testify/assert"
)

func Test_companyDate(t *testing.T) {
	//Copy from NoSQLBooster for MongoDB free edition. This message does not appear if you are using a registered version.
	oid := bson.ObjectIdHex("00000000007543d500000000")
	var b [4]byte
	binary.BigEndian.PutUint32(b[:], uint32(time.Now().Unix()))

	byteOid := []byte(oid)
	for i := 0; i < 4; i++ {
		byteOid[i] = b[i]
	}
	fmt.Println(bson.ObjectId(byteOid[:]))
	assert.True(t, false)
}

func Test_taxFileGetID(t *testing.T) {
	c := Company{
		UnitCode: "12345678",
	}
	oid := c.newID()
	fmt.Println(oid)
	tf := TaxFile{CompanyID: oid, Year: 2007}
	tid := tf.newID()
	fmt.Println(tid)
	assert.True(t, false)
}

func Test_GetCompanyID(t *testing.T) {
	fci, _ := GetCompanyId("12345678", true)
	tci, _ := GetCompanyId("12345678", false)
	fmt.Println(fci, tci)
	assert.True(t, false)
}

func Test_GetEInvoiceID(t *testing.T) {
	einvoice := EInvoice{
		VoucherNumber: "GY73528548",
	}
	fmt.Println(einvoice.newID())
	assert.True(t, false)
}

func Test_IsFakeCompanyID(t *testing.T) {
	//Copy from NoSQLBooster for MongoDB free edition. This message does not appear if you are using a registered version.
	// 0000000045ec110400000000
	// 0000000045c1adfc00000000
	// 0000000000c83ea600000000
	//Copy from NoSQLBooster for MongoDB free edition. This message does not appear if you are using a registered version.
	d := bson.ObjectIdHex("0000000045ec110400000000")
	r := IsFakeCompany(d)
	fmt.Println(r)
	assert.True(t, false)
}
