package doc

import (
	"kalimasi/util"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

type BudgetItem struct {
	Code   string `json:"code"`
	Name   string `json:"name"`
	Amount int    `json:"amount"`
	Desc   string `json:"desc"`
}

type BudgetStatement struct {
	ID        bson.ObjectId `bson:"_id"`
	Title     string
	Typ       string
	Year      int
	StartDate time.Time
	EndDate   time.Time
	CompanyID bson.ObjectId
	Amount    int // 預算總額

	ParentID *bson.ObjectId // 父預算

	Income   []BudgetItem
	Expenses []BudgetItem

	ProjectList []*BudgetStatement `bson:"lookupBudget,omitempty"`

	CommonDoc `bson:"meta"`

	AuditID
	LookUpAudit `bson:"lookupAudit,omitempty"`
}

func (u *BudgetStatement) IsNil() bool {
	return u == nil
}

func (u *BudgetStatement) GetBudgetItem(code, typ string) *BudgetItem {
	bi := &BudgetItem{}
	if typ == TypeAccIncome {
		for _, i := range u.Income {
			if i.Code == code {
				bi.Amount = i.Amount
				bi.Code = i.Code
				bi.Desc = i.Desc
				bi.Name = i.Name
			}
		}
		for _, p := range u.ProjectList {
			pbi := p.GetBudgetItem(code, typ)
			bi.Amount += pbi.Amount
		}
		return bi
	}

	for _, e := range u.Expenses {
		if e.Code == code {
			bi.Amount = e.Amount
			bi.Code = e.Code
			bi.Desc = e.Desc
			bi.Name = e.Name
		}
	}
	for _, p := range u.ProjectList {
		pbi := p.GetBudgetItem(code, typ)
		bi.Amount += pbi.Amount
	}
	return bi
}

func (u *BudgetStatement) GetDoc() interface{} {
	if !u.ID.Valid() {
		u.ID = u.newID()
	}
	return u
}

func (u *BudgetStatement) GetC() string {
	if !u.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(u.CompanyID.Hex(), budgetC)
}

func (u *BudgetStatement) GetID() bson.ObjectId {
	return u.ID
}

func (u *BudgetStatement) SetCompany(c bson.ObjectId) {
	u.CompanyID = c
}

func (u *BudgetStatement) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *BudgetStatement) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *BudgetStatement) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}

func (u *BudgetStatement) GetPipeline(q bson.M) PipelineQryInter {
	a := &Audit{CompanyID: u.CompanyID}
	return &CommonPipeline{
		q: q,
		pipeline: []bson.M{
			bson.M{"$match": q},
			bson.M{
				"$lookup": bson.M{
					"from":         u.GetC(),
					"localField":   "_id",
					"foreignField": "parentid",
					"as":           "lookupBudget",
				},
			},
			bson.M{
				"$lookup": bson.M{
					"from":         a.GetC(),
					"localField":   "auditid",
					"foreignField": "_id",
					"as":           "lookupAudit",
				},
			},
		},
	}
}

const (
	BudgetTypYear    = "year"    // 年度預算
	BudgetTypProject = "project" // 專案預算

	budgetC = "budget"
)

func (b *BudgetStatement) GetMongoIndexes() []mgo.Index {
	return []mgo.Index{
		{
			Key:        []string{"companyid", "typ"},
			Background: true, // can be used while index is being built
		},
		{
			Key:        []string{"parentid"},
			Background: true, // can be used while index is being built
		},
	}
}
