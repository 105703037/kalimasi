package storage

import (
	"errors"
	"io"
)

type Storage interface {
	Save(filePath string, file []byte) (string, error)
	SaveByReader(fp string, reader io.Reader) (string, error)
	Delete(filePath string) error
	Get(filePath string) ([]byte, error)
	FileExist(fp string) bool
	List(dir string) []string
}

type FileStorage struct {
	Location string `yaml:"location"`
	Path     string `yaml:"path"`

	Bucket          string `yaml:"bucket"`
	CredentialsFile string `yaml:"credentialsFile"`

	store Storage
}

func (fs *FileStorage) GetStorage() (Storage, error) {
	if fs.store != nil {
		return fs.store, nil
	}
	switch fs.Location {
	case "hd":
		fs.store = &hd{Path: fs.Path}
	case "firebase":
		fs.store = &myFirebase{
			Bucket:          fs.Bucket,
			CredentialsFile: fs.CredentialsFile,
		}
	default:
		return nil, errors.New("not support location")
	}
	return fs.store, nil
}
