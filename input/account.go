package input

import (
	"errors"

	"github.com/globalsign/mgo/bson"
)

type CreateAccountOpen struct {
	Year          uint16
	DebitAccTerm  []*InputAccTerm `json:"debitAccTermList"`
	CreditAccTerm []*InputAccTerm `json:"creditAccTermList"`
}

func (at *CreateAccountOpen) Validate() error {
	return nil
}

type PutAccountOpen struct {
	CreateAccountOpen
	ID            bson.ObjectId
	DeleteAccterm []bson.ObjectId `json:"deleteAccterm"`
}

func (pb *PutAccountOpen) Validate() error {
	err := pb.CreateAccountOpen.Validate()
	if err != nil {
		return err
	}
	if !pb.ID.Valid() {
		return errors.New("invalid receipt id")
	}
	return nil
}
