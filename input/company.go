package input

import (
	"errors"
	"kalimasi/doc"
	"kalimasi/util"

	"github.com/globalsign/mgo/bson"
)

type CreateCompany struct {
	Typ         string `json:"type"`
	VATnumber   string `json:"vatNumber"`
	Name        string
	AddressInfo struct {
		PostalCode string `json:"postalCode"`
		Country    string `json:"country"`
		City       string `json:"city"`
		Address    string `json:"address"`
	} `json:"addressInfo"`
	Fax     string
	Owner   string
	TaxCode string

	ContactPerson struct {
		Name    string
		Contact struct {
			CtType string `json:"ctType"` // enum: ["office"]
			Value  string
		}
	} `json:"contactPerson"`
}

func (cc *CreateCompany) HasAuditID() bool {
	return false
}

func (cc *CreateCompany) GetAuditUpdateDoc(companyID bson.ObjectId) *doc.AuditUpdateDoc {
	return nil
}

func (cc *CreateCompany) Validate() error {
	if !util.IsStrInList(cc.Typ, doc.CompanyTypEnterprise, doc.CompanyTypOrg) {
		return errors.New("invalid company type")
	}
	if cc.TaxCode == "" {
		return errors.New("missing taxCode")
	}
	if !util.IsVATnumber(cc.VATnumber) {
		return errors.New("invalid vatNumber")
	}
	return nil
}

func (cc CreateCompany) GetAuditType() string {
	return "company-new"
}

func (cc CreateCompany) GetAuditSummary() map[string]interface{} {
	return map[string]interface{}{
		"type":      cc.Typ,
		"name":      cc.Name,
		"vatNumber": cc.VATnumber,
	}
}

type PayAccTermMapConf struct {
	Display     string //付款方式
	AccTermCode string //會計科目
	Enable      bool
}

type PutPayAccTermMapConf struct {
	Display     string //付款方式
	AccTermCode string //會計科目
	Name        string
	Enable      bool
	ID          bson.ObjectId
}

func (pm *PayAccTermMapConf) Validate() error {
	if pm.AccTermCode == "" {
		return errors.New("invalid accTermCode")
	}

	if pm.Display == "" {
		return errors.New("invalid display")
	}

	return nil
}
func (pm *PutPayAccTermMapConf) Validate() error {
	if pm.AccTermCode == "" {
		return errors.New("invalid accTermCode")
	}

	if pm.Name == "" {
		return errors.New("invalid name")
	}

	if pm.Display == "" {
		return errors.New("invalid display")
	}

	return nil
}

func (pm *PayAccTermMapConf) GetAccDoc() *doc.AccMapping {
	m := &doc.AccMapping{
		Enable:      pm.Enable,
		AccTermCode: pm.AccTermCode,
		Display:     pm.Display,
	}

	return m
}

func (pm *PutPayAccTermMapConf) GetAccDoc() *doc.AccMapping {
	m := &doc.AccMapping{
		Enable:      pm.Enable,
		Name:        pm.Name,
		AccTermCode: pm.AccTermCode,
		Display:     pm.Display,
	}

	return m
}

type ConfirmCompanyPerm struct {
	Token    string
	Response string
}

func (ccp *ConfirmCompanyPerm) Validate() error {
	if ccp.Token == "" {
		return errors.New("missing token")
	}
	if !util.IsStrInList(ccp.Response,
		doc.InvitationReponseAccept,
		doc.InvitationReponseDeny,
		doc.InvitationReponseCheck) {
		return errors.New("invalid resopnse")
	}
	return nil
}

type CreateCompanyUserPerm struct {
	CompanyID  bson.ObjectId
	Email      string
	Perm       string
	ConfirmURL string
}

func (ccup *CreateCompanyUserPerm) Validate() error {
	if !ccup.CompanyID.Valid() {
		return errors.New("invalid CID")
	}
	if ok, err := util.IsMail(ccup.Email); !ok {
		return err
	}
	if !util.IsStrInList(ccup.Perm, doc.UserPermOwn, doc.UserPermPro, doc.UserPermUser) {
		return errors.New("invalid perm")
	}
	if ccup.ConfirmURL == "" {
		return errors.New("host not set")
	}
	return nil
}

type PutCompanyUserPerm struct {
	CompanyID bson.ObjectId
	UserID    string
	Perm      string
}

func (pcup *PutCompanyUserPerm) Validate() error {
	if !bson.IsObjectIdHex(pcup.UserID) {
		return errors.New("invalid UID")
	}
	if !util.IsStrInList(pcup.Perm, doc.UserPermOwn, doc.UserPermPro, doc.UserPermUser) {
		return errors.New("invalid perm")
	}
	return nil
}

type PutCompanyUserNote struct {
	CompanyID bson.ObjectId
	UserID    string
	Note      string
}

func (pcup *PutCompanyUserNote) Validate() error {
	if !bson.IsObjectIdHex(pcup.UserID) {
		return errors.New("invalid UID")
	}
	return nil
}

type PutEInvoice struct {
	Enable bool
	Token  string
}

func (pei *PutEInvoice) Validate() error {
	if !pei.Enable {
		return nil
	}
	if pei.Token == "" {
		return errors.New("token not set")
	}
	return nil
}

type PutCompany struct {
	Name        string
	AddressInfo doc.AddressInfo
	Fax         string
	Owner       string
	TaxCode     string `json:"taxCode"`
}

func (pc *PutCompany) Validate() error {
	return nil
}
