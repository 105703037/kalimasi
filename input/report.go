package input

import (
	"errors"
	"kalimasi/doc"
	"kalimasi/report"
	"strconv"
	"time"

	"github.com/globalsign/mgo/bson"
)

type TimeRange struct {
	StartTimeStr string
	EndTimeStr   string

	start, end time.Time
}

func (qb *TimeRange) Validate() error {
	if qb.StartTimeStr != "" && qb.EndTimeStr != "" {
		st, err1 := time.Parse(time.RFC3339, qb.StartTimeStr)
		ed, err2 := time.Parse(time.RFC3339, qb.EndTimeStr)
		if err1 == nil && err2 == nil && st.Unix() > ed.Unix() {

			return errors.New("endDate must bigger than startDate")
		}
		qb.start = st
		qb.end = ed
	}
	return nil
}

type QueryBudgetReport struct {
	Current, Last string
}

func (qb *QueryBudgetReport) Validate() error {
	if !bson.IsObjectIdHex(qb.Current) {
		return errors.New("invalid current")
	}
	if qb.Last != "" && !bson.IsObjectIdHex(qb.Last) {
		return errors.New("invalid last")
	}
	return nil
}

func (qb *QueryBudgetReport) GetMgoQuery() bson.M {
	if qb.Last == "" {
		return bson.M{
			"_id": bson.ObjectIdHex(qb.Current),
		}
	}
	return bson.M{
		"_id": bson.M{"$in": []bson.ObjectId{
			bson.ObjectIdHex(qb.Current),
			bson.ObjectIdHex(qb.Last)}},
	}
}

type QueryFinalAccountReport struct {
	Budget string
}

func (qb *QueryFinalAccountReport) Validate() error {
	if !bson.IsObjectIdHex(qb.Budget) {
		return errors.New("invalid budget")
	}
	return nil
}

func (qb *QueryFinalAccountReport) GetMgoQuery() bson.M {
	return bson.M{
		"_id": bson.ObjectIdHex(qb.Budget),
	}
}

type QueryReportByYearAndTimeRage struct {
	Year string

	year int

	TimeRange
}

func (qb *QueryReportByYearAndTimeRage) GetStartTime() time.Time {
	if qb.start.IsZero() {
		return time.Date(1911+qb.year, 1, 1, 0, 0, 0, 0, time.Now().Location())
	}
	return qb.start
}

func (qb *QueryReportByYearAndTimeRage) GetEndTime() time.Time {
	if qb.end.IsZero() {
		return time.Date(1911+qb.year, 12, 31, 0, 0, 0, 0, time.Now().Location())
	}
	return qb.end
}

func (qb *QueryReportByYearAndTimeRage) GetYear() int {
	if qb.year > 0 {
		return qb.year
	}
	y, _ := strconv.Atoi(qb.Year)
	qb.year = y
	return qb.year
}

func (qb *QueryReportByYearAndTimeRage) Validate() error {
	if qb.Year != "" {
		y, err := strconv.Atoi(qb.Year)
		if err != nil {
			return err
		}
		qb.year = y
	}

	if err := qb.TimeRange.Validate(); err != nil {
		return err
	}
	return nil
}

func (qb *QueryReportByYearAndTimeRage) GetMgoQuery() bson.M {
	q := bson.M{}
	if qb.year > 0 {
		q["year"] = qb.year
	}
	if qb.start.Unix() > 0 && qb.end.Unix() > 0 {
		q["datetime"] = bson.M{"$gte": qb.start, "$lte": qb.end}
	}
	return q
}

type CreateTaxFile struct {
	report.TaxCal
}

func (qb *CreateTaxFile) Validate() error {
	if qb.Year <= 0 {
		return errors.New("invalid year")
	}
	if len(qb.Incomes) == 0 {
		return errors.New("missing incomes")
	}
	if len(qb.Expenditure) == 0 {
		return errors.New("missing expenditure")
	}
	if qb.StartTime.IsZero() {
		return errors.New("missing startTime")
	}
	if qb.EndTime.IsZero() {
		return errors.New("missing endTime")
	}
	if qb.OpenMonth == 0 || qb.OpenMonth > 12 {
		return errors.New("invalid openMonth")
	}
	return nil
}

func (bi *CreateTaxFile) GetDoc(companyID bson.ObjectId) *doc.TaxFile {
	r := &doc.TaxFile{
		Year:              bi.Year,
		StartDate:         bi.StartTime,
		EndDate:           bi.EndTime,
		CompanyID:         companyID,
		Income:            bi.Incomes,
		Expenses:          bi.Expenditure,
		IsTaxFree:         bi.IsTaxFree,
		TaxFreeAmount:     bi.TaxFreeAmount,
		OpenMonth:         bi.OpenMonth,
		TaxCredit:         bi.TaxCredit,
		OverseasTaxCredit: bi.OverseasTaxCredit,
		LostInvoice:       bi.LostInvoice,
	}
	return r
}
