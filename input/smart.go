package input

import (
	"errors"
	"kalimasi/util"
	"strconv"
)

type VatNumInput string

func (vni VatNumInput) Validate() error {
	if len(vni) != 8 {
		return errors.New("invalid length")
	}
	str := string(vni)
	if _, err := strconv.Atoi(str); err != nil {
		return errors.New("should only involve number")
	}

	if util.IsVATnumber(str) == false{
		return errors.New("vatnumber is illegal")
	}
	return nil
}

type ComUpdateInput struct{
	VatNumber string `json:"vatNumber"`
	Company   ComInfo    `json:"company"`
}

type ComInfo struct {
	Name string `json:"name"`
}

func (pi *ComUpdateInput) Validate() error {
	vat := VatNumInput(pi.VatNumber)

	if err := vat.Validate(); err != nil {
		return err
	}
	return nil
}