package util

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func Test_FixStr(t *testing.T) {
	r := FixStrLen("bbb", 8, ' ')
	fmt.Println(r)
	assert.True(t, false)
}

func Test_idtest(t *testing.T) {
	// //
	ok := IsIdNumber("A123456780")
	fmt.Println(ok)
	assert.True(t, false)
}

func Test_GetVoucherNumber(t *testing.T) {
	r := GetVoucherNumber(time.Now(), 1)
	fmt.Println(r)
	assert.True(t, false)
}

func Test_IsVATnumber(t *testing.T) {
	r := IsVATnumber("76356016")
	assert.True(t, r)
}

func Test_IsPhoneNumber(t *testing.T) {
	r, _ := IsLegalPhoneNumber("+886935120080")
	assert.Equal(t, "abc", r)
}
