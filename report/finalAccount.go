/*
 * 收支決算表
 */
package report

import (
	"io"

	"kalimasi/report/pdf"
)

type FinalAccount struct {
	BalanceBudget
}

func (bb *FinalAccount) PDF(w io.Writer) error {
	const subtitle = "收支決算表"
	p := pdf.GetA4PDF(bb.Font, 20, 20, 20, 20)
	p.AddPage()
	style := bb.Style
	// page one report
	p.Text(bb.Title, style.GetTitle(), pdf.AlignCenter)
	p.Br(25)
	p.Text(subtitle, style.GetSubTitle(), pdf.AlignCenter)
	p.Br(25)
	p.Text(bb.getDateRange(), style.GetSubTitle(), pdf.AlignRight)
	p.Br(25)
	p.Line(2)
	p.Br(5)

	nt := pdf.GetNormalTable()
	ha := bb.getHeaderAry()
	for _, h := range ha {
		nt.AddHeader(h)
	}

	for _, i := range bb.Item {
		increaseStr, decreaseStr := "", ""
		if i.Increase > 0 {
			increaseStr = currencyFormat(&i.Increase)
		}
		if i.Decrease > 0 {
			decreaseStr = currencyFormat(&i.Decrease)
		}
		nt.AddRow([]*pdf.TableColumn{
			pdf.GetTableColumn(20, 20, singleAlignCenter, i.Category),
			pdf.GetTableColumn(20, 20, singleAlignCenter, i.SubCategory),
			pdf.GetTableColumn(20, 20, singleAlignCenter, i.Item),
			pdf.GetTableColumn(90, 20, singleAlignCenter, i.AccTerm),
			pdf.GetTableColumn(80, 20, singleAlignRight, currencyFormat(&i.Amount)),
			pdf.GetTableColumn(80, 20, singleAlignRight, currencyFormat(i.LastAmount)),
			pdf.GetTableColumn(75, 20, singleAlignRight, increaseStr),
			pdf.GetTableColumn(75, 20, singleAlignRight, decreaseStr),
			pdf.GetTableColumn(100, 20, singleAlignCenter, i.Desc),
		})
	}
	nt.Draw(&p, style)

	p.Br(5)
	p.Text("(以下請核章)", style.GetSubTitle(), pdf.AlignLeft)
	p.Br(20)
	p.RectFillColor("團體負責人：", style.TextBlock(12), 400, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.RectFillColor("秘書長：", style.TextBlock(12), 100, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.Br(30)
	p.RectFillColor("會計：", style.TextBlock(12), 400, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.RectFillColor("製表：", style.TextBlock(12), 100, 20, pdf.AlignLeft, pdf.ValignMiddle)
	return p.Write(w)
}

func (bb *FinalAccount) getHeaderAry() []pdf.TableHeaderColumn {
	var ha []pdf.TableHeaderColumn
	ha = append(ha,
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"科目"},
				Width:  150,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"決算數"},
				Width:  80,
				Height: 40,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"預算數"},
				Width:  80,
				Height: 40,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"決算與預算比較數"},
				Width:  150,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"說明"},
				Width:  100,
				Height: 40,
			}},
	)
	ha[0].AddSub("款", 20, 20)
	ha[0].AddSub("項", 20, 20)
	ha[0].AddSub("目", 20, 20)
	ha[0].AddSub("科目", 90, 20)

	ha[3].AddSub("增加", 75, 20)
	ha[3].AddSub("減少", 75, 20)

	return ha
}
