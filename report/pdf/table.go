package pdf

import (
	"kalimasi/util"
)

type TableStyle interface {
	Header() TextBlockStyle
	Data() TextBlockStyle
}

type columnData struct {
	TextAry  []string
	StyleAry []TextBlockStyle
}

func (p *PDF) DrawColumn(w, h float64, color Color, rectType string) {
	pdf := p.myPDF
	pdf.SetFillColor(color.R, color.G, color.B)
	pdf.RectFromUpperLeftWithStyle(pdf.GetX(), pdf.GetY(), w, h, rectType)
}

// 文字區塊4個數字
func (p *PDF) TextValuesAry(rows columnData, valign int) {

	pdf := p.myPDF
	ox, x := pdf.GetX(), 0.0

	if ox < p.leftMargin {
		ox = p.leftMargin
	}
	x = ox

	oy, y := pdf.GetY(), pdf.GetY()

	maxFontSize := 8.0

	for i := 0; i < len(rows.TextAry); i++ {
		pdf.SetX(x)
		text := rows.TextAry[i]
		tStyle := rows.StyleAry[i]
		x = p.tableText(text, maxFontSize, tStyle.Color, x, y, tStyle.GetAlign(), valign, tStyle.W, tStyle.W)
	}
	y = oy + maxFontSize
	pdf.SetY(y)
	pdf.SetX(ox)
}

func (p *PDF) tableText(text string, floatFontSize float64, color Color, x, y float64, align, valign int, w, h float64) (endX float64) {
	ox := x
	pdf := p.myPDF
	pdf.SetFillColor(color.R, color.G, color.B)
	if align == AlignCenter {
		textw, _ := pdf.MeasureTextWidth(text)
		x = x + (w / 2) - (textw / 2)
	} else if align == AlignRight {
		textw, _ := pdf.MeasureTextWidth(text)
		x = x + w - textw
	} else {
		x = x + 5
	}

	pdf.SetX(x)

	if valign == ValignMiddle {
		y = y + (h / 2) - (floatFontSize / 2)
	} else if valign == ValignBottom {
		y = y + h - floatFontSize
	}
	pdf.SetY(y)
	pdf.Cell(nil, text)
	endX = ox + w
	return
}

type NormalTable struct {
	header   []TableHeaderColumn
	widthAry []float64
	rows     [][]*TableColumn
}

func GetNormalTable() *NormalTable {
	return &NormalTable{}
}

func (nti *NormalTable) AddHeader(thc TableHeaderColumn) {
	if len(thc.Sub) == 0 {
		nti.widthAry = append(nti.widthAry, thc.Main.Width)
	} else {
		for _, s := range thc.Sub {
			nti.widthAry = append(nti.widthAry, s.Width)
		}
	}

	nti.header = append(nti.header, thc)
}

func (nti *NormalTable) AddRow(r []*TableColumn) {
	nti.rows = append(nti.rows, r)
}

func (nti *NormalTable) Draw(p *PDF, style TableStyle, app ...AddPagePipe) {
	if len(nti.rows) > 0 && len(nti.widthAry) != len(nti.rows[0]) {
		panic("table setting error")
	}
	pdf := p.myPDF
	ox, x := pdf.GetX(), 0.0

	if ox < p.leftMargin {
		ox = p.leftMargin
	}
	x = ox
	pdf.SetX(x)

	//oy, y := pdf.GetY(), pdf.GetY()
	pdf.SetY(pdf.GetY())
	y := pdf.GetY()
	maxHeight := 0.0
	for _, h := range nti.header {
		h.draw(p, style)
		pdf.SetY(y)
		height := h.Main.Height
		if len(h.Sub) > 0 {
			height += h.Sub[0].Height
		}
		if height > maxHeight {
			maxHeight = height
		}
	}
	p.Br(maxHeight)
	rowY := 0.0
	for _, r := range nti.rows {
		maxHeight = 0
		i := 0
		rowY = pdf.GetY()
		for _, h := range r {
			h.Height = h.Height / float64(len(h.Text))
			x = pdf.GetX()
			if maxHeight < h.Height {
				maxHeight = h.Height
			}
			j := 0.0
			aIndex := 0
			for _, text := range h.Text {
				if h.Align[aIndex] == AlignRight {
					text = util.StrAppend(text, "  ")
				}
				pdf.SetY(pdf.GetY() + h.Height*j)
				pdf.SetX(x)
				p.RectFillDrawColor(text, style.Data(), nti.widthAry[i], h.Height, h.Align[aIndex], ValignMiddle)
				j++
				aIndex++
			}
			pdf.SetY(rowY)
			i++
		}
		p.Br(maxHeight)
		if p.height-pdf.GetY() < p.bottomMargin*3 {
			p.Br(10)
			p.AddPage(app...)
		}
	}

}

func (nti *NormalTable) DrawWithPosition(p *PDF, style TableStyle, px, py float64) {
	if len(nti.rows) > 0 && len(nti.widthAry) != len(nti.rows[0]) {
		panic("table setting error")
	}
	pdf := p.myPDF
	ox, x := pdf.GetX(), 0.0

	if px < p.leftMargin {
		px = p.leftMargin
	}
	x = px
	pdf.SetX(x)

	//oy, y := pdf.GetY(), pdf.GetY()
	pdf.SetY(pdf.GetY())
	oy := pdf.GetY()
	y := py
	maxHeight := 0.0
	for _, h := range nti.header {
		h.draw(p, style)
		pdf.SetY(y)
		height := h.Main.Height
		if len(h.Sub) > 0 {
			height += h.Sub[0].Height
		}
		if height > maxHeight {
			maxHeight = height
		}
	}
	p.Br(maxHeight)
	rowY := 0.0
	for _, r := range nti.rows {
		maxHeight = 0
		i := 0
		rowY = p.GetY()
		pdf.SetX(px)
		for _, h := range r {
			h.Height = h.Height / float64(len(h.Text))
			x = p.GetX()
			if maxHeight < h.Height {
				maxHeight = h.Height
			}
			j := 0.0
			aIndex := 0

			for _, text := range h.Text {
				if h.Align[aIndex] == AlignRight {
					text = util.StrAppend(text, "  ")
				}
				pdf.SetY(pdf.GetY() + h.Height*j)
				pdf.SetX(x)
				p.RectFillDrawColor(text, style.Data(), nti.widthAry[i], h.Height, h.Align[aIndex], ValignMiddle)
				j++
				aIndex++
			}
			pdf.SetY(rowY)
			i++
		}
		p.Br(maxHeight)
		if p.height-pdf.GetY() < p.bottomMargin*1.5 {
			p.AddPage()
		}
	}
	pdf.SetX(ox)
	pdf.SetY(oy)
}

type TableColumn struct {
	Text   []string
	Width  float64
	Height float64
	Align  []int
}

func GetTableColumn(w, h float64, align []int, text ...string) *TableColumn {
	tl := len(text)
	if len(align) < tl {
		oldAlign := align[0]
		align = make([]int, len(text))
		for i := 0; i < tl; i++ {
			align[i] = oldAlign
		}
	}
	return &TableColumn{
		Text:   text,
		Width:  w,
		Height: h,
		Align:  align,
	}
}

type TableHeaderColumn struct {
	Main TableColumn
	Sub  []TableColumn
}

func (thc *TableHeaderColumn) AddSub(text string, width, height float64) {
	thc.Sub = append(thc.Sub, TableColumn{Text: []string{text}, Width: width, Height: height})
}

func (nti *TableHeaderColumn) draw(p *PDF, style TableStyle) {
	pdf := p.myPDF
	ox, x := pdf.GetX(), 0.0

	if ox < p.leftMargin {
		ox = p.leftMargin
	}

	x = ox
	pdf.SetX(x)
	pdf.SetY(pdf.GetY())
	p.RectFillDrawColor(nti.Main.Text[0], style.Header(), nti.Main.Width, nti.Main.Height, AlignCenter, ValignMiddle)
	if len(nti.Sub) == 0 {
		return
	}
	pdf.SetY(pdf.GetY() + nti.Main.Height)
	maxSubHeight := 0.0
	for _, t := range nti.Sub {
		pdf.SetX(x)
		p.RectFillDrawColor(t.Text[0], style.Header(), t.Width, t.Height, AlignCenter, ValignMiddle)
		x = x + t.Width
		if maxSubHeight < t.Height {
			maxSubHeight = t.Height
		}
	}
	pdf.SetY(pdf.GetY() + maxSubHeight)

}
