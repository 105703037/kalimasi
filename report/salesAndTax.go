/*
 * 營業稅 營業人銷售額與稅額申報書檔 (401, 403, 404)
 */
package report

import (
	"errors"
	"io"
	"kalimasi/report/pdf"
	"time"
)

const (
	TypeSalesAndTax401 = 1
	TypeSalesAndTax403 = 3
	TypeSalesAndTax404 = 4

	TypeDeclareMonth  = 2
	TypeDeclarePeriod = 1
	fileno            = "00000000"
)

var cityMap = map[string][1]byte{
	"Taipei":         [1]byte{'A'},
	"Taichung":       [1]byte{'B'},
	"Keelung":        [1]byte{'C'},
	"Tainan":         [1]byte{'D'},
	"Kaohsiung":      [1]byte{'E'},
	"NewTaipei":      [1]byte{'F'},
	"Yilan":          [1]byte{'G'},
	"Taoyuan":        [1]byte{'H'},
	"Chiayi":         [1]byte{'I'},
	"HsinchuCountry": [1]byte{'J'},
	"Miaoli":         [1]byte{'K'},
	//"TaichungCountry": [1]byte{'L'},
	"Nantou":       [1]byte{'M'},
	"Changhua":     [1]byte{'N'},
	"Hsinchu":      [1]byte{'O'},
	"Yunlin":       [1]byte{'P'},
	"ChiayiCounty": [1]byte{'Q'},
	//"TainanCounty": [1]byte{'R'},
	//"KaohsiungCounty": [1]byte{'S'},
	"Pingtung":   [1]byte{'T'},
	"Hualien":    [1]byte{'U'},
	"Taitung":    [1]byte{'V'},
	"Kinmen":     [1]byte{'W'},
	"Penghu":     [1]byte{'X'},
	"Lienchiang": [1]byte{'Z'},
}

type SalesAndTax struct {
	DataType           uint8 // 資料別
	VATnumber          [8]byte
	StartDate, EndDate time.Time
	Times              uint8   // 申報次數
	TaxNumber          [9]byte // 稅籍編號
	Code               uint8   // 總繳代號
	Typ                uint8   // 申報種類(1按期申報，2按月申報)
	CityCode           [1]byte // 縣市別
	// 應稅銷售額
	Code1  int64 // S9(12) 三聯式電子計算機發票
	Code5  int64 // S9(12) 收銀機發票(三聯式)
	Code9  int64 // S9(12) 二聯式收銀機(二聯式)發票
	Code13 int64 // S9(12) 免用發票
	Code17 int64 // S9(12) 退回及折讓
	Code21 int64 // S9(12) 合計
	// 應稅稅額
	Code2  int64 // S9(10) 三聯式電子計算機發票
	Code6  int64 // S9(10) 收銀機發票(三聯式)
	Code10 int64 // S9(10) 二聯式收銀機(二聯式)發票
	Code14 int64 // S9(10) 免用發票
	Code18 int64 // S9(10) 退回及折讓
	Code22 int64 // S9(10) 合計

	Code82 int64 // S9(12) 免開立統一發票銷售額
	// 零稅率銷售額
	Code7  int64 // S9(12) 非經海關出口應附證明文件者
	Code15 int64 // S9(12) 經海關出口免附證明文件者
	Code19 int64 // S9(12) 退回及折讓
	Code23 int64 // S9(12) 合計
	// 免稅銷售額
	Code4  int64 // S9(12) 三聯式電子計算機發票
	Code8  int64 // S9(12) 收銀機發票(三聯式)
	Code12 int64 // S9(12) 二聯式收銀機(二聯式)發票
	Code16 int64 // S9(12) 免用發票
	Code20 int64 // S9(12) 退回及折讓
	Code24 int64 // S9(12) 合計
	// 特種稅額-特種飲食
	Code52 int64 // S9(12) 25% - 銷售額
	Code53 int64 // S9(10) 25% - 稅額
	Code54 int64 // S9(12) 15% - 銷售額
	Code55 int64 // S9(10) 15% - 稅額
	// 特種稅額-銀行、保險及信託投資業
	Code56 int64 // S9(12) 專屬本業收入 2% - 銷售額
	Code57 int64 // S9(10) 專屬本業收入 2% - 稅額
	Code58 int64 // S9(12) 非專屬本業收入 5% - 銷售額
	Code59 int64 // S9(10) 非專屬本業收入 5% - 稅額
	Code60 int64 // S9(12) 再保收入 1% - 銷售額
	Code61 int64 // S9(10) 再保收入 1% - 稅額
	// 特種稅額
	Code62 int64 // S9(12) 免稅收入 - 銷售額
	// 退回及折讓
	Code63 int64 // S9(12) 銷售額
	Code64 int64 // S9(10) 稅額
	// 合計
	Code65 int64 // S9(12) 銷售額
	Code66 int64 // S9(10) 稅額
	// 銷售額分析
	Code25 int64 // S9(12) 銷售額總計
	Code26 int64 // S9(12) 土地
	Code27 int64 // S9(12) 其他固定之資產
	//應比例計算得扣抵進項金額
	Code28 int64 // S9(12) 統一發票扣抵聯 - 進貨及費用
	Code30 int64 // S9(12) 統一發票扣抵聯 - 固定資產
	Code32 int64 // S9(12) 三聯式收銀機發票扣抵聯 - 進貨及費用
	Code34 int64 // S9(12) 三聯式收銀機發票扣抵聯 - 固定資產
	Code36 int64 // S9(12) 載有稅額之其他憑證 - 進貨及費用
	Code38 int64 // S9(12) 載有稅額之其他憑證 - 固定資產
	Code40 int64 // S9(12) 退出及折讓 - 進貨及費用
	Code42 int64 // S9(12) 退出及折讓 - 固定資產
	Code44 int64 // S9(12) 合計 - 進貨及費用
	Code46 int64 // S9(12) 合計 - 固定資產
	// 應比例計算得扣抵進項稅額
	Code29 int64 // S9(10) 統一發票扣抵聯 - 進貨及費用
	Code31 int64 // S9(10) 統一發票扣抵聯 - 固定資產
	Code33 int64 // S9(10) 三聯式收銀機發票扣抵聯 - 進貨及費用
	Code35 int64 // S9(10) 三聯式收銀機發票扣抵聯 - 固定資產
	Code37 int64 // S9(10) 載有稅額之其他憑證 - 進貨及費用
	Code39 int64 // S9(10) 載有稅額之其他憑證 - 固定資產
	Code41 int64 // S9(10)  退出及折讓 - 進貨及費用
	Code43 int64 // S9(10) 應比例計算得扣抵進項稅額 退出及折讓 - 固定資產
	// 兼營營業人查填
	Code50 uint8 // 9(3) 不得扣抵比例
	Code51 int64 // S9(10) 得扣抵之進項稅額
	Code78 int64 // S9(12) 海關代徵營業稅繳納證扣抵聯 進貨及費用金額
	Code80 int64 // S9(12) 海關代徵營業稅繳納證扣抵聯 固定資產金額
	Code73 int64 // S9(12) 進口免稅貨物
	Code74 int64 // S9(12) 購買國外勞務給付額之購買國外勞務稅額計算
	Code79 int64 // S9(10) 海關代徵營業稅繳納證扣抵聯 進貨及費用稅額
	Code81 int64 // S9(10) 海關代徵營業稅繳納證扣抵聯 固定資產稅額
	Code75 int64 // S9(10) 應比例計算進項稅額之購買國 外勞務稅額計算
	Code76 int64 // S9(10) 應納稅額之購買國外勞務稅額計算
	// 稅額計算
	Code101 int64 // S9(10) 本(期)月銷項稅額合計
	Code103 int64 // S9(10) 購買國外勞務應納稅額
	Code104 int64 // S9(10) 特種稅額計算應納稅額
	Code105 int64 // S9(10) 中途歇業年底調整補徵應繳稅額
	Code106 int64 // S9(10) 小計(1+3+4+5)
	Code107 int64 // S9(10) 得扣抵進項稅額合計
	Code108 int64 // S9(10) 上期(月)累積留抵稅額
	Code109 int64 // S9(10) 中途歇業年底調整應退稅額
	Code110 int64 // S9(10) 小計(7+8+9)
	Code111 int64 // S9(10) 本期(月)應實繳稅額(6-10)
	Code112 int64 // S9(10) 本期(月)申報留抵稅額(10-6)
	Code113 int64 // S9(10) 得退稅限額合計
	Code114 int64 // S9(10) 本期(月)應退稅額
	Code115 int64 // S9(10) 本期(月)累積留抵稅額

	Style reportStyle       `json:"-"`
	Font  map[string]string `json:"-"`
}

func (bb *SalesAndTax) SetVATnumber(v string) error {
	l := len(v)
	if l != 8 {
		return errors.New("VATnumber length must be 8")
	}
	for i := 0; i < l; i++ {
		bb.VATnumber[i] = v[i]
	}
	return nil
}

func (bb *SalesAndTax) SetTaxNumber(v string) error {
	l := len(v)
	if l != 9 {
		return errors.New("TaxNumber length must be 9")
	}
	for i := 0; i < l; i++ {
		bb.TaxNumber[i] = v[i]
	}
	return nil
}

func (bb *SalesAndTax) PDF(w io.Writer) error {
	p := pdf.GetA4PDF(bb.Font, 20, 20, 20, 20)
	p.AddPage()
	style := bb.Style
	// page one report
	//p.Text("aaaaa", style.GetSubTitle(), pdf.AlignCenter)
	//p.Br(25)
	p.TextWithPosition("tst", style.GetTitle(), 100, 50)
	return p.Write(w)
}

const char0 = 48

func (bb *SalesAndTax) TXT(w io.Writer) error {
	data := make([]byte, 1080)
	for i := 0; i < len(data); i++ {
		data[i] = char0
	}
	data[0] = char0 + bb.DataType
	for i := 0; i < len(bb.VATnumber); i++ {
		data[11+i] = bb.VATnumber[i]
	}
	_, err := w.Write(data)
	if err != nil {
		return err
	}
	return nil
}
