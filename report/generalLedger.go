package report

import (
	"errors"
	"fmt"
	"io"
	"kalimasi/doc"
	"kalimasi/report/pdf"
	"kalimasi/util"
	"strconv"
	"time"
)

type GeneralLedgerStyle interface {
	reportStyle
}

type GLSource interface {
	GetAccountCodes() []string
	GetAccountName(code string) string
	PageData(code string, page int) []*doc.Account
	GetSubTotalPage(code string) int
	TotalDebit(code string) int
	TotalCredit(code string) int
	OldBalance(code string) int
}

type GeneralLedger struct {
	Name               string
	Owner              string
	Year               int
	No                 int
	Page               int
	StartDate, EndDate time.Time

	DataSource GLSource

	Style GeneralLedgerStyle `json:"-"`
	Font  map[string]string  `json:"-"`

	commonReport
}

func (bb *GeneralLedger) GetFileName() string {
	return fmt.Sprintf("generalLedger_%d_%s", bb.Year, bb.Name)
}

func (bb *GeneralLedger) PDF(w io.Writer) error {
	if bb.DataSource == nil {
		return errors.New("datasource not set")
	}
	const title = "總分類帳"
	p := pdf.GetA4PDF(bb.Font, 20, 20, 40, 40)
	p.AddPage()
	style := bb.Style
	// page one report
	// 封面頁
	const titleHeight = 102
	p.Br((p.GetHeight() - titleHeight) / 3)
	p.RectFillDrawColor(title, style.TextBlock(62), p.GetWidth(), titleHeight, pdf.AlignCenter, pdf.ValignMiddle)
	p.Br(150)
	p.Text(bb.Name, style.GetTitle(), pdf.AlignCenter)
	p.Br(60)
	p.Text(fmt.Sprintf("%d 年度", bb.Year), style.GetTitle(), pdf.AlignCenter)
	// 啟用表
	p.AddPage()
	const startTableTitle = "帳簿啟用表"
	p.RectFillDrawColor(startTableTitle, style.TextBlock(48), p.GetWidth(), 62, pdf.AlignCenter, pdf.ValignMiddle)
	p.Br(62)
	const title1Width, title2Width = 110, 300
	title3Width := p.GetWidth() - title1Width - title2Width
	p.RectFillDrawColor("團體名稱", style.TextBlock(14), title1Width, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.RectFillDrawColor(bb.Name, style.TextBlock(14), title2Width, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.RectFillDrawColor("團體及負責人印鑑", style.TextBlock(14), title3Width, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.Br(20)
	p.RectFillDrawColor("負責人姓名", style.TextBlock(14), title1Width, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.RectFillDrawColor(bb.Owner, style.TextBlock(14), title2Width, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.RectFillDrawColor("", style.TextBlock(14), title3Width, 7*20, pdf.AlignLeft, pdf.ValignMiddle)
	p.Br(20)
	p.RectFillDrawColor("帳簿名稱", style.TextBlock(14), title1Width, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.RectFillDrawColor(title, style.TextBlock(14), title2Width, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.Br(20)
	p.RectFillDrawColor("帳簿編號", style.TextBlock(14), title1Width, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.RectFillDrawColor(fmt.Sprintf("第　　　%d　　　號", bb.No), style.TextBlock(14), title2Width, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.Br(20)
	p.RectFillDrawColor("帳簿冊數", style.TextBlock(14), title1Width, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.RectFillDrawColor(fmt.Sprintf("第　　%d　　冊。共　　%d　　冊", bb.No, bb.No), style.TextBlock(14), title2Width, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.Br(20)
	p.RectFillDrawColor("帳簿頁數", style.TextBlock(14), title1Width, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.RectFillDrawColor(fmt.Sprintf("計　　　%d　　　頁", bb.Page), style.TextBlock(14), title2Width, 20, pdf.AlignLeft, pdf.ValignMiddle)
	p.Br(20)
	p.RectFillDrawColor("使用　　日期", style.TextBlock(14), title1Width, 40, pdf.AlignLeft, pdf.ValignMiddle)
	p.RectFillDrawColor("　　中華民國　　　年　　　月　　　日", style.TextBlock(14), title2Width, 40, pdf.AlignLeft, pdf.ValignMiddle)

	p.TextWithPosition("起", style.GetSubTitle(), 60, 225)
	p.TextWithPosition("迄", style.GetSubTitle(), 60, 245)

	p.TextWithPosition("自", style.GetSubTitle(), 135, 225)
	p.TextWithPosition("至", style.GetSubTitle(), 135, 245)

	p.TextWithPosition("起", style.GetSubTitle(), 410, 225)
	p.TextWithPosition("至", style.GetSubTitle(), 410, 245)

	p.TextWithPosition(strconv.Itoa(bb.StartDate.Year()-1911), style.GetContent(), 230, 225)
	p.TextWithPosition(strconv.Itoa(bb.EndDate.Year()-1911), style.GetContent(), 230, 245)
	p.TextWithPosition(strconv.Itoa(int(bb.StartDate.Month())), style.GetContent(), 290, 225)
	p.TextWithPosition(strconv.Itoa(int(bb.EndDate.Month())), style.GetContent(), 290, 245)
	p.TextWithPosition(strconv.Itoa(bb.StartDate.Day()), style.GetContent(), 345, 225)
	p.TextWithPosition(strconv.Itoa(bb.EndDate.Day()), style.GetContent(), 345, 245)
	p.Br(40)
	const subSectionManagerTitle = "經管人員"
	p.RectFillDrawColor(subSectionManagerTitle, style.TextBlock(28), p.GetWidth(), 30, pdf.AlignCenter, pdf.ValignMiddle)
	p.Br(30)
	nt := pdf.GetNormalTable()
	ha := getManagerHeaderAry(p.GetWidth())
	for _, h := range ha {
		nt.AddHeader(h)
	}
	for i := 0; i < 3; i++ {
		nt.AddRow([]*pdf.TableColumn{
			pdf.GetTableColumn(20, 30, singleAlignCenter, ""),
			pdf.GetTableColumn(20, 30, singleAlignCenter, ""),
			pdf.GetTableColumn(20, 30, singleAlignCenter, ""),
			pdf.GetTableColumn(20, 30, singleAlignCenter, ""),
			pdf.GetTableColumn(20, 30, singleAlignCenter, ""),
			pdf.GetTableColumn(20, 30, singleAlignCenter, ""),
			pdf.GetTableColumn(20, 30, singleAlignCenter, ""),
			pdf.GetTableColumn(20, 30, singleAlignCenter, ""),
			pdf.GetTableColumn(20, 30, singleAlignCenter, ""),
		})
	}
	nt.Draw(&p, style)
	const subSectionReviewTitle = "審核人員"
	p.RectFillDrawColor(subSectionReviewTitle, style.TextBlock(28), p.GetWidth(), 30, pdf.AlignCenter, pdf.ValignMiddle)
	p.Br(30)
	nt.Draw(&p, style)
	p.RectFillDrawColor("粘貼印花稅票處", style.TextBlock(18), p.GetWidth(), 160, pdf.AlignCenter, pdf.ValignMiddle)
	// 目錄
	p.AddPage()
	p.RectFillColor(bb.Name, style.TextBlock(28), p.GetWidth(), 30, pdf.AlignCenter, pdf.ValignMiddle)
	p.Br(30)
	p.RectFillColor("【　目　　錄　】", style.TextBlock(22), p.GetWidth(), 30, pdf.AlignCenter, pdf.ValignMiddle)
	p.Br(60)

	list := bb.DataSource.GetAccountCodes()
	catalogPage := 1
	const rowCountLimit = 30
	totalRowCount := 3 * rowCountLimit
	rowCount := 0
	tableCount := 0.0
	for _, l := range list {
		if rowCount >= rowCountLimit {
			nt.DrawWithPosition(&p, style, p.GetX()+180*tableCount, p.GetY())
			tableCount++
			rowCount = 0
		}
		if rowCount == 0 {
			nt = pdf.GetNormalTable()
			ha = getCatalogHeaderAry()
			for _, h := range ha {
				nt.AddHeader(h)
			}
		}
		subtotalPage := bb.DataSource.GetSubTotalPage(l)
		if subtotalPage == 0 {
			continue
		}
		nt.AddRow([]*pdf.TableColumn{
			pdf.GetTableColumn(20, 20, singleAlignCenter, bb.DataSource.GetAccountName(l)),
			pdf.GetTableColumn(20, 20, singleAlignCenter, strconv.Itoa(catalogPage)),
		})
		catalogPage += subtotalPage
		rowCount++
		totalRowCount--
	}
	for i := 0; i < totalRowCount; i++ {
		if rowCount >= rowCountLimit {
			nt.DrawWithPosition(&p, style, p.GetX()+180*tableCount, p.GetY())
			tableCount++
			rowCount = 0
		}
		if rowCount == 0 {
			nt = pdf.GetNormalTable()
			ha = getCatalogHeaderAry()
			for _, h := range ha {
				nt.AddHeader(h)
			}
		}
		nt.AddRow([]*pdf.TableColumn{
			pdf.GetTableColumn(20, 20, singleAlignCenter, ""),
			pdf.GetTableColumn(20, 20, singleAlignCenter, ""),
		})
		rowCount++
	}
	if rowCount > 0 {
		nt.DrawWithPosition(&p, style, p.GetX()+180*tableCount, p.GetY())
		tableCount++
		rowCount = 0
	}

	// 分類帳頁
	acs := list

	var amountDebit, amountCredit *int

	typ, desc := "", ""
	currentPage, subTotalPage := 0, 0

	createDetailHeader := func(accCode, accName string, page int) {
		p.AddPage()
		currentPage++
		subTotalPage = bb.DataSource.GetSubTotalPage(accCode)
		p.TextWithPosition(bb.Name, style.GetSubTitle(), 20, 20)
		p.LineWithPosition(2, 20, 38, 200, 38)
		p.TextWithPosition(fmt.Sprintf("%d 年度 總分類帳", bb.Year), style.GetTitle(), 20, 45)
		p.LineWithPosition(2, 20, 72, 200, 72)
		p.TextWithPosition(
			fmt.Sprintf("自 %d 年 %d 月 %d 日 至 %d 年 %d 月 %d 日 止",
				bb.StartDate.Year()-1911, int(bb.StartDate.Month()), bb.StartDate.Day(),
				bb.EndDate.Year()-1911, int(bb.EndDate.Month()), bb.EndDate.Day(),
			),
			style.GetContent(), 20, 80)
		p.TextWithPosition("會計科目：", style.GetContent(), 20, 100)
		p.TextWithPosition(util.StrAppend(accCode, " ", accName), style.GetSubTitle(), 80, 100)

		p.RectDrawColorWithPosition(fmt.Sprintf("總　頁：第 %d 頁", currentPage), style.TextBlock(14), 150, 20, pdf.AlignLeft, pdf.ValignMiddle, 350, 20)
		p.RectDrawColorWithPosition(fmt.Sprintf("科目頁：第 %d ~ %d 頁", subTotalPage, page), style.TextBlock(14), 150, 20, pdf.AlignLeft, pdf.ValignMiddle, 350, 40)

		p.RectFillColorWithPosition("上期餘額：", style.TextBlock(12), 100, 20, pdf.AlignLeft, pdf.ValignMiddle, 350, 60)
		ob := bb.DataSource.OldBalance(accCode)
		p.RectFillColorWithPosition(currencyFormat(&ob), style.TextBlock(12), 80, 20, pdf.AlignRight, pdf.ValignMiddle, 420, 60)

		p.RectFillColorWithPosition("借方合計：", style.TextBlock(12), 100, 20, pdf.AlignLeft, pdf.ValignMiddle, 350, 80)
		td := bb.DataSource.TotalDebit(accCode)
		p.RectFillColorWithPosition(currencyFormat(&td), style.TextBlock(12), 80, 20, pdf.AlignRight, pdf.ValignMiddle, 420, 80)
		p.RectFillColorWithPosition("貸方合計：", style.TextBlock(12), 100, 20, pdf.AlignLeft, pdf.ValignMiddle, 350, 100)
		tc := bb.DataSource.TotalCredit(accCode)
		p.RectFillColorWithPosition(currencyFormat(&tc), style.TextBlock(12), 80, 20, pdf.AlignRight, pdf.ValignMiddle, 420, 100)
		p.RectFillColorWithPosition("本期餘額：", style.TextBlock(12), 100, 20, pdf.AlignLeft, pdf.ValignMiddle, 350, 120)
		balance := ob + td - tc
		typ := "借"
		if balance < 0 {
			typ = "貸"
			balance = balance * -1
		}
		p.RectFillColorWithPosition(util.StrAppend(currencyFormat(&balance), "(", typ, ")"), style.TextBlock(12), 80, 20, pdf.AlignRight, pdf.ValignMiddle, 420, 120)
		p.Br(100)
	}

	for _, ac := range acs {
		page := 1
		subtotal := 0
		for details := bb.DataSource.PageData(ac, page); details != nil; details = bb.DataSource.PageData(ac, page) {
			createDetailHeader(ac, bb.DataSource.GetAccountName(ac), page)
			nt = pdf.GetNormalTable()
			ha = getDetailHeaderAry(p.GetWidth())
			for _, h := range ha {
				nt.AddHeader(h)
			}
			page = page + 1
			for _, d := range details {
				desc = d.Summary
				switch d.Typ {
				case doc.TypeAccountDebit:
					amountDebit = &d.Amount
					amountCredit = nil
					subtotal += d.Amount
					typ = "借"
				case doc.TypeAccountCredit:
					amountDebit = nil
					amountCredit = &d.Amount
					subtotal -= d.Amount
					typ = "貸"
				}
				nt.AddRow([]*pdf.TableColumn{
					pdf.GetTableColumn(20, 20, singleAlignCenter, d.DateTime.Format("2006-01-02")),
					pdf.GetTableColumn(20, 20, singleAlignCenter, d.VoucherNumber),
					pdf.GetTableColumn(20, 20, singleAlignLeft, desc),
					pdf.GetTableColumn(20, 20, singleAlignRight, currencyFormat(amountDebit)),
					pdf.GetTableColumn(20, 20, singleAlignRight, currencyFormat(amountCredit)),
					pdf.GetTableColumn(20, 20, singleAlignRight, currencyFormat(&subtotal)),
					pdf.GetTableColumn(20, 20, singleAlignCenter, typ),
				})
			}
			nt.Draw(&p, style)
		}
	}
	return p.Write(w)
}

func getCatalogHeaderAry() []pdf.TableHeaderColumn {
	var ha []pdf.TableHeaderColumn
	columnWidth := []float64{120, 40}
	ha = append(ha,
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"科目名稱"},
				Width:  columnWidth[0],
				Height: 30,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"頁次"},
				Width:  columnWidth[1],
				Height: 30,
			}},
	)
	return ha
}

func getDetailHeaderAry(width float64) []pdf.TableHeaderColumn {
	var ha []pdf.TableHeaderColumn
	columnWidth := []float64{50, 60, width - 340, 70, 70, 70, 20}
	ha = append(ha,
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"日期"},
				Width:  columnWidth[0],
				Height: 30,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"傳票編號"},
				Width:  columnWidth[1],
				Height: 30,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"摘要內容"},
				Width:  columnWidth[2],
				Height: 30,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"借方金額"},
				Width:  columnWidth[3],
				Height: 30,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"貸方金額"},
				Width:  columnWidth[4],
				Height: 30,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"餘額"},
				Width:  columnWidth[5],
				Height: 30,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"借貸"},
				Width:  20,
				Height: 30,
			}},
	)
	return ha
}

func getManagerHeaderAry(width float64) []pdf.TableHeaderColumn {
	var ha []pdf.TableHeaderColumn
	ha = append(ha,
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"職別"},
				Width:  80,
				Height: 40,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"姓名"},
				Width:  100,
				Height: 40,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"簽字或蓋章"},
				Width:  width - 420,
				Height: 40,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"接管"},
				Width:  120,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"轉交"},
				Width:  120,
				Height: 20,
			}},
	)
	ha[3].AddSub("年", 40, 20)
	ha[3].AddSub("月", 40, 20)
	ha[3].AddSub("日", 40, 20)
	ha[4].AddSub("年", 40, 20)
	ha[4].AddSub("月", 40, 20)
	ha[4].AddSub("日", 40, 20)

	return ha
}

func (gl GeneralLedger) TotalCount() int {
	const pageNumber = 20.0
	total := 0
	cl := gl.DataSource.GetAccountCodes()
	for _, g := range cl {
		total += gl.DataSource.GetSubTotalPage(g)
	}
	return total
}

type Ledger struct {
	AccName string
	AccCode string

	AccList []*doc.Account
}

type GLStyle string

func (ds GLStyle) GetTitle() pdf.TextStyle {
	return pdf.TextStyle{
		Font:     "tw-m",
		FontSize: 16,
		Color:    pdf.ColorBlack,
	}
}

func (ds GLStyle) GetSubTitle() pdf.TextStyle {
	return pdf.TextStyle{
		Font:     "tw-r",
		FontSize: 12,
		Color:    pdf.ColorBlack,
	}
}

func (ds GLStyle) GetContent() pdf.TextStyle {
	return pdf.TextStyle{
		Font:     "tw-r",
		FontSize: 12,
		Color:    pdf.ColorBlack,
	}
}

func (ds GLStyle) Header() pdf.TextBlockStyle {
	return pdf.TextBlockStyle{
		TextStyle: pdf.TextStyle{
			Font:     "tw-m",
			FontSize: 9,
			Color:    pdf.ColorBlack,
		},
		BackGround: pdf.ColorWhite,
	}
}

func (ds GLStyle) Data() pdf.TextBlockStyle {
	return pdf.TextBlockStyle{
		TextStyle: pdf.TextStyle{
			Font:     "tw-r",
			FontSize: 9,
			Color:    pdf.ColorBlack,
		},
		W:          30,
		H:          10.0,
		BackGround: pdf.ColorWhite,
		TextAlign:  "left",
	}
}

func (ds GLStyle) TextBlock(size int) pdf.TextBlockStyle {
	return pdf.TextBlockStyle{
		TextStyle: pdf.TextStyle{
			Font:     "tw-m",
			FontSize: size,
			Color:    pdf.ColorBlack,
		},
		W:          30,
		H:          10.0,
		BackGround: pdf.ColorWhite,
		TextAlign:  "left",
	}
}

func (ds GLStyle) UTextBlock(size int) pdf.TextBlockStyle {
	return pdf.TextBlockStyle{
		TextStyle: pdf.TextStyle{
			Font:     "tw-m",
			FontSize: size,
			Style:    "U",
			Color:    pdf.ColorBlack,
		},
		W:          30,
		H:          10.0,
		BackGround: pdf.ColorWhite,
		TextAlign:  "left",
	}
}
